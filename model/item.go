package model

type Item struct {
	ItemId int64  `json:"itemid"`
	Name   string `json:"name"`
}

type Detail struct {
	ItemId   int64  `json:"itemid"`
	Name     string `json:"name"`
	PhotoUrl string `json:"photo_url"`
	Desc     string `json:"desc"`
}
