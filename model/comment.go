package model

type Comment struct {
	Cid         int64 `json:"cid"`
	Content     string `json:"content"`
	PbName      string `json:"pbname"`
	ReplyTo     int64 `json:"reply_to"`
	PublishDate int64 `json:"pb_date"`
}
