package controller

import (
	"fmt"
	"mini_market/config"
	"mini_market/container"
	"mini_market/container/ucfactory"
	"mini_market/controller/handlers"
	"mini_market/logger"
	"mini_market/usecases"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/pkg/errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"

	cpb "mini_market/controller/protobuf/comment"
	ipb "mini_market/controller/protobuf/item"
	upb "mini_market/controller/protobuf/user"
)

type GrpcServer struct {
	ctn    container.Container
	server *grpc.Server
	quitCh chan os.Signal
}

// getUsecase build an usecase service by key
func (g *GrpcServer) getUsecase(key string) interface{} {
	builder, found := ucfactory.GetUseCaseBuilder(key)
	if !found {
		logger.Log.Panicf("builder of %s not found", key)
	}
	srv, err := builder.Build(g.ctn, g.ctn.GetConfig(), key)
	if err != nil {
		logger.Log.Panicf("cannot build use case[key=%s]: %s", key, err.Error())
	}
	return srv
}

func NewGrpcServer(ctn container.Container) *GrpcServer {
	g := &GrpcServer{}
	g.ctn = ctn
	auth := &handlers.AuthInterceptor{UserSrv: g.getUsecase(config.UserSrv).(usecases.UserServices)}
	param := keepalive.ServerParameters{
		MaxConnectionIdle: time.Duration(ctn.GetConfig().ServerCfg.MaxConnIdleSec) * time.Second,
		Timeout:           time.Duration(ctn.GetConfig().ServerCfg.ConnTimeoutSec) * time.Second,
	}
	opts := []grpc.ServerOption{
		grpc.StreamInterceptor(auth.StreamAuthInterceptor()),
		grpc.UnaryInterceptor(auth.UnaryAuthInterceptor()),
		grpc.KeepaliveParams(param),
	}
	g.server = grpc.NewServer(opts...)

	userHandler := &handlers.UserHandler{Srv: g.getUsecase(config.UserSrv).(usecases.UserServices)}
	upb.RegisterUserSrvServer(g.server, userHandler)

	itemHandler := &handlers.ItemHandler{Srv: g.getUsecase(config.ItemSrv).(usecases.ItemServices)}
	ipb.RegisterItemSrvServer(g.server, itemHandler)

	cmtHandler := &handlers.CmtHandler{Srv: g.getUsecase(config.CmtSrv).(usecases.CommentService)}
	cpb.RegisterCmtSrvServer(g.server, cmtHandler)

	g.quitCh = make(chan os.Signal)
	signal.Notify(g.quitCh, syscall.SIGINT, syscall.SIGTERM)
	return g
}

func (g *GrpcServer) Run(addr string) {
	lis, err := net.Listen("tcp", addr)
	logger.Log.Infof("start grpc server on addr: %s", addr)
	if err != nil {
		logger.Log.Panicf("%s", errors.WithStack(err))
		fmt.Println("grpc server not running")
	}

	go func() {
		if err := g.server.Serve(lis); err != nil {
			logger.Log.Panicf("%s", errors.WithStack(err))
		}
	}()
	// listen for os signal and close the server gracefully
	<-g.quitCh
	logger.Log.Infof("shutting down gprc server ...")
	go func() {
		g.server.GracefulStop()
	}()
	<-time.After(5 * time.Second)

	logger.Log.Infof("finalizing container")
	if err := g.ctn.Finalize(); err != nil {
		logger.Log.Errorf("fail to finalize container: %s", errors.WithStack(err))
	}
}
