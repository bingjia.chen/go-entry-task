// Package user provides grpc handler for user service
package handlers

import (
	"context"
	pb "mini_market/controller/protobuf/user"
	"mini_market/model"
	"mini_market/usecases"

	"google.golang.org/grpc"
)

type UserHandler struct {
	pb.UnimplementedUserSrvServer
	Srv usecases.UserServices
}

func toUserModel(req *pb.LoginRequest) *model.User {
	return &model.User{
		Username: req.Username,
		Password: []byte(req.Passwd),
	}
}

func (u *UserHandler) Register(ctx context.Context, req *pb.LoginRequest) (*pb.LoginReply, error) {
	user := toUserModel(req)
	token, err := u.Srv.Register(user)
	if err != nil {
		grpc.SetTrailer(ctx, getErrMsg(err))
		return &pb.LoginReply{}, nil
	}
	grpc.SetTrailer(ctx, OKMsg())
	return &pb.LoginReply{Token: token}, nil
}

func (u *UserHandler) Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginReply, error) {
	user := toUserModel(req)
	token, err := u.Srv.Login(user)
	if err != nil {
		grpc.SetTrailer(ctx, getErrMsg(err))
		return &pb.LoginReply{}, nil
	}
	grpc.SetTrailer(ctx, OKMsg())
	return &pb.LoginReply{Token: token}, nil
}
