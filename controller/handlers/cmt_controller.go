package handlers

import (
	"context"
	pb "mini_market/controller/protobuf/comment"
	"mini_market/errcode"
	"mini_market/logger"
	"mini_market/model"
	"mini_market/usecases"
	"strconv"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type CmtHandler struct {
	pb.UnimplementedCmtSrvServer
	Srv usecases.CommentService
}

func toCommentMsg(cmt *model.Comment) *pb.Comment {
	return &pb.Comment{
		Id:      cmt.Cid,
		Pber:    cmt.PbName,
		ReplyTo: cmt.ReplyTo,
		Content: cmt.Content,
		Pbtime:  cmt.PublishDate,
	}
}
func (c *CmtHandler) AddComment(ctx context.Context, req *pb.AddRequest) (*pb.AddReply, error) {
	md, found := metadata.FromIncomingContext(ctx)
	if !found || len(md.Get("uid")) == 0 {
		logger.Log.Debugf("cmt[] AddComment: uid not found")
		grpc.SetTrailer(ctx, getErrMsg(errcode.ErrPermissionDenied))
		return &pb.AddReply{}, nil
	}
	uid, err := strconv.ParseInt(md.Get("uid")[0], 10, 64)
	if uid <= 0 {
		grpc.SetTrailer(ctx, getErrMsg(errcode.ErrPermissionDenied))
		return &pb.AddReply{}, nil
	}
	id, err := c.Srv.AddComment(uid, req.ReplyTo, req.CommentOn, req.Content)
	if err != nil {
		grpc.SetTrailer(ctx, getErrMsg(err))
		logger.Log.Errorf("AddComment[%+v] fail: %s", req, err)
		return &pb.AddReply{}, nil
	}
	grpc.SetTrailer(ctx, OKMsg())
	return &pb.AddReply{Id: id}, nil
}

func (c *CmtHandler) GetComments(req *pb.CommentRequest, stream pb.CmtSrv_GetCommentsServer) error {
	cmts, err := c.Srv.GetCommentOfItem(req.Id, req.Page)
	if err != nil {
		stream.SetTrailer(getErrMsg(err))
		return nil
	}
	for _, cmt := range cmts {
		err = stream.Send(toCommentMsg(cmt))
		if err != nil {
			stream.SetTrailer(getErrMsg(err))
			return nil
		}
	}
	stream.SetTrailer(OKMsg())
	return nil
}

func (c *CmtHandler) GetReplyOfComment(req *pb.CommentRequest, stream pb.CmtSrv_GetReplyOfCommentServer) error {
	cmts, err := c.Srv.GetReplyOfComment(req.Id)
	if err != nil {
		stream.SetTrailer(getErrMsg(err))
		return nil
	}
	for _, cmt := range cmts {
		err = stream.Send(toCommentMsg(cmt))
		if err != nil {
			stream.SetTrailer(getErrMsg(err))
			return nil
		}
	}
	stream.SetTrailer(OKMsg())
	return nil
}
