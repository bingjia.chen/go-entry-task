package handlers

import (
	"context"
	pb "mini_market/controller/protobuf/item"
	"mini_market/model"
	"mini_market/usecases"
	"strconv"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type ItemHandler struct {
	pb.UnimplementedItemSrvServer
	Srv usecases.ItemServices
}

func toItemMsg(item *model.Item) *pb.Item {
	return &pb.Item{
		ItemId: item.ItemId,
		Name:   item.Name,
	}
}

func toDetailMsg(detail *model.Detail) *pb.DetailReply {
	return &pb.DetailReply{
		ItemId:   detail.ItemId,
		Name:     detail.Name,
		PhotoUrl: detail.PhotoUrl,
		Desc:     detail.Desc,
	}
}

func (i *ItemHandler) GetItems(req *pb.ItemRequest, stream pb.ItemSrv_GetItemsServer) error {
	items, err := i.Srv.GetItems(req.Page)
	if err != nil {
		stream.SetTrailer(getErrMsg(err))
		return nil
	}
	for _, item := range items {
		if err := stream.Send(toItemMsg(item)); err != nil {
			stream.SetTrailer(getErrMsg(err))
			return err
		}
	}
	stream.SetTrailer(OKMsg())
	return nil
}

func (i *ItemHandler) Search(req *pb.SearchRequest, stream pb.ItemSrv_SearchServer) error {
	switch req.Type {
	case pb.SearchRequest_ByTag:
		items, next, err := i.Srv.GetItemsByTags(req.Key, req.Payload)
		if err != nil {
			stream.SetTrailer(getErrMsg(err))
			stream.SetTrailer(metadata.Pairs("next", strconv.FormatInt(next, 10)))
			return nil
		}
		for _, item := range items {
			if err := stream.Send(toItemMsg(item)); err != nil {
				stream.SetTrailer(getErrMsg(err))
				stream.SetTrailer(metadata.Pairs("next", strconv.FormatInt(next, 10)))
				return err
			}
		}
		stream.SetTrailer(OKMsg())
		stream.SetTrailer(metadata.Pairs("next", strconv.FormatInt(next, 10)))
		return nil
	case pb.SearchRequest_ByName:
		item, err := i.Srv.GetItemByName(req.Key)
		if err != nil {
			stream.SetTrailer(getErrMsg(err))
			return nil
		}
		if item != nil {
			err = stream.Send(toItemMsg(item))
			if err != nil {
				stream.SetTrailer(getErrMsg(err))
				return err
			}
		}
	}
	stream.SetTrailer(OKMsg())
	return nil
}

func (i *ItemHandler) GetDetail(ctx context.Context, req *pb.DetailRequest) (*pb.DetailReply, error) {
	detail, err := i.Srv.GetDetailById(req.ItemId)
	if err != nil {
		grpc.SetTrailer(ctx, getErrMsg(err))
		return &pb.DetailReply{}, nil
	}
	grpc.SetTrailer(ctx, OKMsg())
	return toDetailMsg(detail), nil
}
