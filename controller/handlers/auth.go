// Package main provides ...
package handlers

import (
	"context"
	"mini_market/errcode"
	"mini_market/logger"
	"mini_market/usecases"
	"strconv"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

// AuthInterceptor is a middleware for authencation
type AuthInterceptor struct {
	UserSrv usecases.UserServices
}

// noAuthService contains services that can be access without token
var noAuthService = map[string]struct{}{
	"/ItemSrv/GetItems": {},
	"/UserSrv/Login":    {},
	"/UserSrv/Register": {},
}

func (a *AuthInterceptor) getToken(ctx context.Context) (string, error) {
	md, found := metadata.FromIncomingContext(ctx)
	if !found {
		return "", errcode.ErrPermissionDenied
	}
	token := md.Get("token")
	if len(token) == 0 {
		return "", errcode.ErrPermissionDenied
	}
	return token[0], nil
}

func (a *AuthInterceptor) UnaryAuthInterceptor() grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler) (interface{}, error) {
		// logger.Log.Infof("call method %s", info.FullMethod)
		_, in := noAuthService[info.FullMethod]
		if in {
			return handler(ctx, req)
		}
		token, err := a.getToken(ctx)
		if err != nil {
			grpc.SetTrailer(ctx, getErrMsg(err))
			return nil, nil
		}
		uid, err := a.UserSrv.CheckAuth(token)
		if err != nil {
			grpc.SetTrailer(ctx, getErrMsg(err))
			return nil, nil
		}
		// logger.Log.Infof("got uid %d", uid)
		ctx = metadata.NewIncomingContext(ctx, metadata.Pairs("uid", strconv.FormatInt(uid, 10)))
		return handler(ctx, req)
	}
}

type wrapperStream struct {
	s   grpc.ServerStream
	ctx context.Context
}

func newWrapperStream(s grpc.ServerStream, extraCtx context.Context) *wrapperStream {
	return &wrapperStream{s, extraCtx}
}

func (w *wrapperStream) SendHeader(md metadata.MD) error {
	return w.s.SendHeader(md)
}

func (w *wrapperStream) SetHeader(md metadata.MD) error {
	return w.s.SetHeader(md)
}

// SetTrailer sets the trailer metadata which will be sent with the RPC status.
// When called more than once, all the provided metadata will be merged.
func (w *wrapperStream) SetTrailer(md metadata.MD) {
	w.s.SetTrailer(md)
}

// Context returns the context for this stream.
func (w *wrapperStream) Context() context.Context {
	return w.ctx
}

func (w *wrapperStream) SendMsg(m interface{}) error {
	return w.s.SendMsg(m)
}
func (w *wrapperStream) RecvMsg(m interface{}) error {
	return w.s.RecvMsg(m)
}

func (a *AuthInterceptor) StreamAuthInterceptor() grpc.StreamServerInterceptor {
	return func(
		srv interface{},
		stream grpc.ServerStream,
		info *grpc.StreamServerInfo,
		handler grpc.StreamHandler,
	) error {
		logger.Log.Infof("call method %s", info.FullMethod)
		_, in := noAuthService[info.FullMethod]
		if in {
			return handler(srv, stream)
		}
		token, err := a.getToken(stream.Context())
		if err != nil {
			stream.SetTrailer(getErrMsg(err))
			return nil
		}
		uid, err := a.UserSrv.CheckAuth(token)
		if err != nil {
			stream.SetTrailer(getErrMsg(err))
			return nil
		}
		ctx := metadata.NewIncomingContext(stream.Context(), metadata.Pairs("uid", strconv.FormatInt(uid, 10)))
		return handler(srv, newWrapperStream(stream, ctx))
	}
}
