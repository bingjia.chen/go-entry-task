package handlers

import (
	"mini_market/errcode"
	"strconv"

	"google.golang.org/grpc/metadata"
)

func OKMsg() metadata.MD {
	return getErrMsg(errcode.ErrOK)
}

func getErrMsg(err error) metadata.MD {
	code, ok := err.(errcode.ErrCode)
	if !ok {
		code = errcode.ErrUnknown
	}
	return metadata.Pairs(
		"errcode", strconv.Itoa(int(code)),
		"errmsg", code.String(),
	)
}
