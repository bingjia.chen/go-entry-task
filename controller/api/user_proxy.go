// Package userapi provides a proxy service for grpc service
package api

import (
	"context"
	pb "mini_market/controller/protobuf/user"
	"mini_market/errcode"
	"mini_market/logger"
	"net/http"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type UserProxy struct {
	Conn *grpc.ClientConn
}

// Register registers an user
// Path: /user/register
// Method: Post
// Params:
//	 username - {string} username
//   passwd - {string} password
// Returns:
//	 token - {string} token
func (u *UserProxy) Register(c *gin.Context) {
	var param struct {
		Username string `json:"username"`
		Password string `json:"passwd"`
	}
	if err := c.BindJSON(&param); err != nil {
		logger.Log.Debugf("gin register fail: invalid arugument")
		c.JSON(http.StatusOK, gin.H{"token": "", "errno": errcode.ErrInvalidArgument})
		return
	}

	stub := pb.NewUserSrvClient(u.Conn)
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	var md metadata.MD
	reply, err := stub.Register(ctx, &pb.LoginRequest{Username: param.Username, Passwd: param.Password}, grpc.Trailer(&md))
	if err != nil {
		logger.Log.Errorf("gin register fail: %s", err)
		c.JSON(http.StatusInternalServerError, gin.H{"token": "", "errno": errcode.ErrInternal})
		return
	}

	errno, err := GetErrcode(md)
	if err != nil {
		logger.Log.Errorf("gin register fail: %s", err)
		c.JSON(http.StatusInternalServerError, gin.H{"token": "", "errno": errcode.ErrInternal})
	}
	c.JSON(http.StatusOK, gin.H{"token": reply.Token, "errno": errno})
}

// Login allows an user to login
// Path: /user/login
// Method: Post
// Params:
//	 username - {string} username
//   password - {string} password
// Return:
//	 token - {string} token
func (u *UserProxy) Login(c *gin.Context) {
	var param struct {
		Username string `json:"username"`
		Password string `json:"passwd"`
	}
	if err := c.BindJSON(&param); err != nil {
		logger.Log.Debugf("gin login fail: invalid arugument")
		c.JSON(http.StatusOK, gin.H{"token": "", "errno": errcode.ErrInvalidArgument})
		return
	}
	stub := pb.NewUserSrvClient(u.Conn)
	var md metadata.MD
	reply, err := stub.Login(context.Background(), &pb.LoginRequest{Username: param.Username, Passwd: param.Password}, grpc.Trailer(&md))
	if err != nil {
		logger.Log.Errorf("gin login fail: %s,", err)
		c.JSON(http.StatusInternalServerError, gin.H{"token": "", "errno": errcode.ErrInternal})
		return
	}
	errno, err := GetErrcode(md)
	if err != nil {
		logger.Log.Errorf("gin login fail: %s", err)
		c.JSON(http.StatusInternalServerError, gin.H{"token": "", "errno": errcode.ErrInternal})
	}
	c.JSON(http.StatusOK, gin.H{"token": reply.Token, "errno": errno})
}
