package api

import (
	"context"
	"io"
	pb "mini_market/controller/protobuf/item"
	"mini_market/errcode"
	"mini_market/model"
	"net/http"
	"strconv"

	"log"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type ItemProxy struct {
	Conn *grpc.ClientConn
}

func itemToModel(item *pb.Item) *model.Item {
	return &model.Item{
		ItemId: item.ItemId,
		Name:   item.Name,
	}
}

func detailToModel(detail *pb.DetailReply) *model.Detail {
	return &model.Detail{
		ItemId:   detail.ItemId,
		Name:     detail.Name,
		PhotoUrl: detail.PhotoUrl,
		Desc:     detail.Desc,
	}
}

// GetItems return a page of items
// Path: /item?page=[int]
// Params:
//	 page {int} - page index
// Returns:
//	 errno {int} - error number
//	 items {model.Item[]} - array of items
// NOTE: this api need no token
func (i *ItemProxy) GetItems(c *gin.Context) {
	pageStr := c.DefaultQuery("page", "0")
	page, err := strconv.ParseInt(pageStr, 10, 64)
	if err != nil || page < 0 {
		c.JSON(http.StatusOK, gin.H{"items": "", "errno": errcode.ErrInvalidArgument})
		return
	}

	stub := pb.NewItemSrvClient(i.Conn)
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	var md metadata.MD
	stream, err := stub.GetItems(ctx, &pb.ItemRequest{Page: page}, grpc.Trailer(&md))
	if err != nil {
		log.Printf("gin[page=%d] fail to call rpc: %s", page, err)
		c.JSON(http.StatusOK, gin.H{"items": "", "errno": errcode.ErrInternal})
		return
	}

	items := make([]*model.Item, 0, 20)
	for {
		item, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Printf("gin[page=%d] fail to receive from grpc server: %v", page, err)
			c.JSON(http.StatusOK, gin.H{"items": "", "errno": errcode.ErrUnknown})
			return
		}
		items = append(items, itemToModel(item))
	}
	errno, err := GetErrcode(md)
	if err != nil {
		log.Printf("gin [page=%d] GetItems fail: errcode not found", page)
		c.JSON(http.StatusInternalServerError, gin.H{"items": "", "errno": errcode.ErrInternal})
		return
	}
	c.JSON(http.StatusOK, gin.H{"items": items, "errno": errno})
}

// GetDetail return the detail of an item by its id
// Path: /item/detail?itemid=[int]
// Params:
//	 itemid {int} - id of the item
// Returns:
//	 errno {int} - error number
//	 detail {model.Detial} - the detail of item
// NOTE: this api need token
func (i *ItemProxy) GetDetail(c *gin.Context) {
	idStr := c.Query("itemid")
	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil || id <= 0 {
		c.JSON(http.StatusOK, gin.H{"detail": "", "errno": errcode.ErrInvalidArgument})
		return
	}
	token := c.GetHeader("Authorization")
	if len(token) == 0 {
		c.JSON(http.StatusOK, gin.H{"detail": "", "errno": errcode.ErrPermissionDenied})
		return
	}

	stub := pb.NewItemSrvClient(i.Conn)
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	var md metadata.MD
	ctx = metadata.AppendToOutgoingContext(ctx, "token", token)

	detail, err := stub.GetDetail(ctx, &pb.DetailRequest{ItemId: id}, grpc.Trailer(&md))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"detail": "", "errno": errcode.ErrInternal})
		return
	}
	errno, err := GetErrcode(md)
	if err != nil {
		log.Printf("gin[id=%d] GetDetail fail: errcode not found", id)
		c.JSON(http.StatusInternalServerError, gin.H{"detail": "", "errno": errcode.ErrInternal})
		return
	}
	if errno != errcode.ErrOK {
		c.JSON(http.StatusOK, gin.H{"detail": "", "errno": errno})
		return
	}
	c.JSON(http.StatusOK, gin.H{"detail": detailToModel(detail), "errno": errno})
}

// Search search for items by name or tag
// Path: /item/search?key=[string]&type=[int]&lastid=[int]
// Parmas:
//	 key {string} - the key for searching
//   typ {string} - by "name" or by "tag"
//   next {int} - next cursor(only available for searching by tag)
// Returns:
//   items {model.Item[]} - searching result
//   next {int} - next cursor
//	 errno {int} - error number
func (i *ItemProxy) Search(c *gin.Context) {
	var params struct {
		Key    string `form:"key"`
		Typ    string `form:"type"`
		LastId int64  `form:"lastid"`
	}
	err := c.ShouldBind(&params)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"items": "", "next": 0, "errno": errcode.ErrInvalidArgument})
		return
	}
	token := c.GetHeader("Authorization")
	if len(token) == 0 {
		c.JSON(http.StatusOK, gin.H{"items": "", "next": 0, "errno": errcode.ErrPermissionDenied})
		return
	}

	var typ pb.SearchRequest_Type
	switch params.Typ {
	case "name":
		typ = pb.SearchRequest_ByName
	case "tag":
		typ = pb.SearchRequest_ByTag
	}

	stub := pb.NewItemSrvClient(i.Conn)

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	var md metadata.MD
	ctx = metadata.AppendToOutgoingContext(ctx, "token", token)
	stream, err := stub.Search(ctx, &pb.SearchRequest{Key: params.Key, Type: typ, Payload: params.LastId}, grpc.Trailer(&md))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"items": "", "next": 0, "errno": errcode.ErrInternal})
		return
	}
	items := make([]*model.Item, 0, 20)
	for {
		item, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Printf("gin[params=%+v] fail to receive from grpc server: %s", params, err)
			c.JSON(http.StatusOK, gin.H{"items": "", "next": 0, "errno": errcode.ErrUnknown})
			return
		}
		items = append(items, itemToModel(item))
	}
	errno, err := GetErrcode(md)
	if err != nil {
		log.Printf("gin[params=%+v] Search fail: errcode not found ", params)
		c.JSON(http.StatusInternalServerError, gin.H{"items": "", "next": 0, "errno": errcode.ErrInternal})
		return
	}
	next := md.Get("next")

	c.JSON(http.StatusOK, gin.H{"items": items, "next": next[0], "errno": errno})
}
