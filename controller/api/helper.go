package api

import (
	"errors"
	"mini_market/errcode"
	"strconv"
	"time"

	"google.golang.org/grpc/metadata"
)

const timeout = 2 * time.Second

func GetErrcode(trailer metadata.MD) (errcode.ErrCode, error) {
	data := trailer.Get("errcode")
	if len(data) == 0 {
		return -1, errors.New("errcode not found")
	}
	code, err := strconv.Atoi(data[0])
	if err != nil {
		return -1, errors.New("invalid errcode")
	}
	return errcode.ErrCode(code), nil
}
