package api

import (
	"context"
	"io"
	pb "mini_market/controller/protobuf/comment"
	"mini_market/errcode"
	"mini_market/logger"
	"mini_market/model"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type CommentProxy struct {
	Conn *grpc.ClientConn
}

func cmtToModel(cmt *pb.Comment) *model.Comment {
	return &model.Comment{
		Cid:         cmt.Id,
		Content:     cmt.Content,
		PbName:      cmt.Pber,
		ReplyTo:     cmt.ReplyTo,
		PublishDate: cmt.Pbtime,
	}
}

// AddComment publishes a comment
// Path: /comment/
// Method: Post
// Params:
//	content {string} - the comment content
//  comment_on {int} - itemId which the comment comments on
//  reply_to {int} - the commentId which the comment replis to
// Returns:
//  comment_id {int}
//  errno {int}
func (p *CommentProxy) AddComment(c *gin.Context) {
	var params struct {
		Content   string `json:"content"`
		CommentOn int64  `json:"comment_on"`
		ReplyTo   int64  `json:"reply_to"`
	}

	err := c.BindJSON(&params)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"comment_id": "", "errno": errcode.ErrInvalidArgument})
		return
	}
	if params.Content == "" {
		panic("content is blank")
	}
	token := c.GetHeader("Authorization")
	if len(token) == 0 {
		c.JSON(http.StatusOK, gin.H{"comment_id": "", "errno": errcode.ErrPermissionDenied})
		return
	}

	stub := pb.NewCmtSrvClient(p.Conn)

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	var md metadata.MD
	ctx = metadata.AppendToOutgoingContext(ctx, "token", token)
	reply, err := stub.AddComment(ctx, &pb.AddRequest{
		Content:   params.Content,
		ReplyTo:   params.ReplyTo,
		CommentOn: params.CommentOn,
	}, grpc.Trailer(&md))

	if err != nil {
		logger.Log.Errorf("gin[params=%+v] call AddComment fail: %v", params, err)
		c.JSON(http.StatusServiceUnavailable, gin.H{"comment_id": "", "errno": errcode.ErrInternal})
		return
	}
	errno, err := GetErrcode(md)
	if err != nil {
		logger.Log.Errorf("gin[params=%+v] AddComment fail: errcode not found ", params)
		c.JSON(http.StatusInternalServerError, gin.H{"comment_id": "", "errno": errcode.ErrInternal})
		return
	}
	c.JSON(http.StatusOK, gin.H{"comment_id": reply.Id, "errno": errno})
	return
}

// GetComments get comments of an specified item by its id
// Path: /comment?itemid=[int]&page=[int]
// Params:
//	 itemid - {int} the id of item
// Returns:
//	 cmts - {Comment[]} comments of the item
//   errno - {int} error number
func (p *CommentProxy) GetComments(c *gin.Context) {
	var err error
	itemid, found := c.GetQuery("itemid")
	if !found {
		c.JSON(http.StatusOK, gin.H{"cmts": "", "errno": errcode.ErrInvalidArgument})
		return
	}
	page := c.DefaultQuery("page", "0")
	req := &pb.CommentRequest{}

	req.Id, err = strconv.ParseInt(itemid, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"cmts": "", "errno": errcode.ErrInvalidArgument})
		return
	}
	req.Page, err = strconv.ParseInt(page, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"cmts": "", "errno": errcode.ErrInvalidArgument})
		return
	}
	token := c.GetHeader("Authorization")
	if token == "" {
		c.JSON(http.StatusUnauthorized, gin.H{"cmts": "", "errno": errcode.ErrPermissionDenied})
		return
	}

	stub := pb.NewCmtSrvClient(p.Conn)
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	var md metadata.MD
	ctx = metadata.AppendToOutgoingContext(ctx, "token", token)
	stream, err := stub.GetComments(ctx, req, grpc.Trailer(&md))
	if err != nil {
		logger.Log.Errorf("gin [%+v] cannot call GetComments: %s", req, err)
		c.JSON(http.StatusInternalServerError, gin.H{"cmts": "", "errno": errcode.ErrInternal})
		return
	}
	cmts := make([]*model.Comment, 0, 20)
	for {
		cmt, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			logger.Log.Errorf("gin[%+v] fail to receive from grpc server: %s", req, err)
			c.JSON(http.StatusOK, gin.H{"cmts": cmts, "errno": errcode.ErrUnknown})
			return
		}
		cmts = append(cmts, cmtToModel(cmt))
	}
	errno, err := GetErrcode(md)
	if err != nil {
		logger.Log.Errorf("gin [%+v] GetComments fail: errcode not found", req)
		c.JSON(http.StatusInternalServerError, gin.H{"cmt": "", "errno": errcode.ErrInternal})
		return
	}
	c.JSON(http.StatusOK, gin.H{"cmts": cmts, "errno": errno})
}

// GetReply get replies of an specified comment
// Path: /comment/reply?cmtid={int}
// Params:
//	 cmtid - {int} the id of comment
// Returns:
//	 cmts - {Comment[]} reply of the comment
//   errno - {int} error number
func (p *CommentProxy) GetReply(c *gin.Context) {
	var err error
	itemid, found := c.GetQuery("cmtid")
	if !found {
		c.JSON(http.StatusOK, gin.H{"cmts": "", "errno": errcode.ErrInvalidArgument})
		return
	}
	req := &pb.CommentRequest{}

	req.Id, err = strconv.ParseInt(itemid, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"cmts": "", "errno": errcode.ErrInvalidArgument})
		return
	}
	token := c.GetHeader("Authorization")
	if token == "" {
		c.JSON(http.StatusUnauthorized, gin.H{"cmts": "", "errno": errcode.ErrPermissionDenied})
		return
	}

	stub := pb.NewCmtSrvClient(p.Conn)
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	var md metadata.MD
	ctx = metadata.AppendToOutgoingContext(ctx, "token", token)
	stream, err := stub.GetReplyOfComment(ctx, req, grpc.Trailer(&md))
	if err != nil {
		logger.Log.Errorf("gin [%+v] cannot call GetReplyOfComment: %s", req, err)
		c.JSON(http.StatusInternalServerError, gin.H{"cmts": "", "errno": errcode.ErrInternal})
		return
	}
	cmts := make([]*model.Comment, 0, 20)
	for {
		cmt, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			logger.Log.Errorf("gin[%+v] fail to receive comments from grpc server: %s", req, err)
			c.JSON(http.StatusOK, gin.H{"cmts": cmts, "errno": errcode.ErrUnknown})
			return
		}
		cmts = append(cmts, cmtToModel(cmt))
	}
	errno, err := GetErrcode(md)
	if err != nil {
		logger.Log.Errorf("gin [%+v] GetReplyOfComment fail: errcode not found", req)
		c.JSON(http.StatusInternalServerError, gin.H{"cmts": "", "errno": errcode.ErrInternal})
		return
	}
	c.JSON(http.StatusOK, gin.H{"cmts": cmts, "errno": errno})
}
