// Package controller provides grpc service as the adapter to the usecase
// An Http API server implemented with gin is also privdede
package controller

import (
	"context"
	"errors"
	"log"
	"mini_market/controller/api"
	"mini_market/logger"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/ratelimit"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
)

// var ginPort = flag.String("n", "", "server port")
// var grpcAddr = flag.String("g", "", "grpc server address")

var retryPolicy = `{
"methodConfig": [{
	"name": [{"service": "/UserSrv/Login"}],
	"waitForReady": true,

	"retryPolicy": {
		"MaxAttempts": 3,
		"InitialBackoff": "1.0s",
		"MaxBackoff": "1.s",
		"BackoffMultiplier": 1.0,
		"RetryableStatusCodes": ["UNAVAILABLE", "DEADLINE_EXCEEDED"]
	}
}]
}`

type GinServer struct {
	engine   *gin.Engine
	ginAddr  string
	grpcAddr string
	conns    []*grpc.ClientConn
	rl       ratelimit.Limiter
}

func NewGinServer(ginAddr, grpcAddr string) *GinServer {
	server := &GinServer{}
	eg := gin.New()
	server.engine = eg
	server.ginAddr = ginAddr
	server.grpcAddr = grpcAddr
	server.setupRouter()
	server.engine.Use(server.limiter(600, 1000))
	server.engine.Use(server.errorLogger())
	server.rl = ratelimit.New(1000)

	http.DefaultTransport.(*http.Transport).MaxConnsPerHost = 1000
	return server
}

// error logger for gin
func (g *GinServer) errorLogger() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()
		if len(c.Errors) != 0 {
			logger.Log.Errorf("[gin] error: %v", c.Errors)
		}
	}
}

// rate limiter middleware
func (g *GinServer) limiter(eventPerSec, maxBucketSize int) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		g.rl.Take()
		ctx.Next()
	}
}

func (g *GinServer) setupRouter() {
	conn, err := grpc.Dial(g.grpcAddr,
		grpc.WithInsecure(),
		grpc.WithDefaultServiceConfig(retryPolicy),
		grpc.WithKeepaliveParams(keepalive.ClientParameters{
			Time:                5 * time.Minute,
			Timeout:             10 * time.Minute,
			PermitWithoutStream: true}),
	)
	if err != nil {
		log.Fatalf("Fail to start grpc connection: %v", err)
	}

	user := &api.UserProxy{Conn: conn}
	g.engine.POST("/user/register", user.Register)
	g.engine.POST("/user/login", user.Login)
	g.conns = append(g.conns, conn)

	conn, err = grpc.Dial(g.grpcAddr, grpc.WithInsecure())
	item := &api.ItemProxy{Conn: conn}
	if err != nil {
		log.Fatalf("Fail to start grpc connection: %v", err)
	}
	g.conns = append(g.conns, conn)
	g.engine.GET("/item", item.GetItems)
	g.engine.GET("/item/detail", item.GetDetail)
	g.engine.GET("/item/search", item.Search)

	conn, err = grpc.Dial(g.grpcAddr, grpc.WithInsecure())
	g.conns = append(g.conns, conn)
	if err != nil {
		log.Fatalf("Fail to start grpc connection: %v", err)
	}

	conn, err = grpc.Dial(g.grpcAddr, grpc.WithInsecure())
	g.conns = append(g.conns, conn)
	item = &api.ItemProxy{Conn: conn}
	if err != nil {
		log.Fatalf("Fail to start grpc connection: %v", err)
	}
	cmt := &api.CommentProxy{Conn: conn}
	g.engine.POST("/comment", cmt.AddComment)
	g.engine.GET("/comment", cmt.GetComments)
	g.engine.GET("/comment/reply", cmt.GetReply)
}

func (g *GinServer) Run() {
	srv := &http.Server{
		Addr:         g.ginAddr,
		Handler:      g.engine,
		ReadTimeout:  60 * time.Second,
		WriteTimeout: 60 * time.Second,
		IdleTimeout:  240 * time.Second,
	}
	quit := make(chan os.Signal)
	go func() {
		log.Printf("start http proxy server on: %s\n", g.ginAddr)
		if err := srv.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			log.Printf("listen: %v\n", err)
		}
	}()

	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutting down http proxy server...")
	for i := 0; i < len(g.conns); i++ {
		g.conns[i].Close()
	}

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Http proxy server forced to shutdown:", err)
	}

	log.Println("Http proxy server exiting")
}
