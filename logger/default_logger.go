package logger

import (
	"log"
	"mini_market/config"
)

type DefaultLogger struct {
	Turned int
}

const (
	debugf = (1 << 4)
	infof  = (1 << 3)
	warnf  = (1 << 2)
	errorf = (1 << 1)
	panicf = (1 << 0)
)

func NewDefaultLogger(cfg *config.AppConfig) *DefaultLogger {
	dl := new(DefaultLogger)
	var level string
	var i int
	if cfg.LogCfg == nil {
		level = config.Info
	}
	switch level {
	case config.Debug:
		i = 5
	case config.Info:
		i = 4
	case config.Warn:
		i = 3
	case config.Error:
		i = 2
	case config.Panic:
		i = 1
	default:
		i = 4
	}
	dl.Turned = (1 << i) - 1
	return dl
}

func (d *DefaultLogger) Debugf(format string, args ...interface{}) {
	if (d.Turned & debugf) != 0 {
		log.Printf("[Debug]"+format, args...)
	}
}

func (d *DefaultLogger) Infof(format string, args ...interface{}) {
	if (d.Turned & infof) != 0 {
		log.Printf("[Info]"+format, args...)
	}
}

func (d *DefaultLogger) Warnf(format string, args ...interface{}) {
	if (d.Turned & warnf) != 0 {
		log.Printf("[Warn]"+format, args...)
	}
}

func (d *DefaultLogger) Errorf(format string, args ...interface{}) {
	if (d.Turned & errorf) != 0 {
		log.Printf("[Error]"+format, args...)
	}
}

func (d *DefaultLogger) Panicf(format string, args ...interface{}) {
	if (d.Turned & panicf) != 0 {
		log.Printf("[Panicf]"+format, args...)
	}
}
