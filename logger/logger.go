// package logger provides a global logger
// and a Logger interface which is an adapter for adapting some libraries like zap
package logger

var Log Logger

type Logger interface {
	Infof(format string, args ...interface{})
	Warnf(format string, args ...interface{})
	Debugf(format string, args ...interface{})
	Panicf(format string, args ...interface{})
	Errorf(format string, args ...interface{})
}

// SetLogger should be called if you want to use a custom logger
// If not called, an default logger using standard library will be used
func SetLogger(newLogger Logger) {
	Log = newLogger
}
