package logger

import (
	"encoding/json"
	"io/ioutil"
	"mini_market/config"

	"github.com/pkg/errors"
	"go.uber.org/zap"
)

// ZapLogger is a wrapper for zap library
type ZapLogger struct {
	logger *zap.SugaredLogger
}

func (z *ZapLogger) Errorf(format string, args ...interface{}) {
	z.logger.Errorf(format, args...)
}

func (z *ZapLogger) Infof(format string, args ...interface{}) {
	z.logger.Infof(format, args...)
}

func (z *ZapLogger) Warnf(format string, args ...interface{}) {
	z.logger.Warnf(format, args...)
}

func (z *ZapLogger) Debugf(format string, args ...interface{}) {
	z.logger.Debugf(format, args...)
}

func (z *ZapLogger) Panicf(format string, args ...interface{}) {
	z.logger.Panicf(format, args...)
}

// customizeLog applies the configuration to cfg
func customizeLog(cfg *zap.Config, lc *config.LogConfig) error {
	cfg.DisableCaller = !lc.EnableCaller

	l := zap.NewAtomicLevel().Level()
	err := l.Set(lc.Level)
	if err != nil {
		return errors.Wrap(err, "customizeLog")
	}
	cfg.Level.SetLevel(l)
	return nil
}

// initZapLogger reads config file and generates zap.Logger
func initZapLogger(lc *config.LogConfig) (*zap.Logger, error) {
	if lc.ConfigFileName == "" {
		return zap.L(), nil
	}

	cf, err := ioutil.ReadFile(lc.ConfigFileName)
	if err != nil {
		return nil, errors.Wrap(err, "fail to read zap config file")
	}

	var cfg zap.Config
	if err := json.Unmarshal(cf, &cfg); err != nil {
		return nil, errors.Wrap(err, "fail to parse zap config file")
	}
	if err := customizeLog(&cfg, lc); err != nil {
		return nil, err
	}
	if logger, err := cfg.Build(); err != nil {
		return nil, errors.Wrap(err, "fail to build logger")
	} else {
		return logger, nil
	}
}

// NewZapLogger return a new ZapLogger
func NewZapLogger(lc *config.LogConfig) (Logger, error) {
	zl, err := initZapLogger(lc)
	if err != nil {
		return nil, err
	}
	defer zl.Sync()

	sl := zl.Sugar()
	sl.Info()

	return &ZapLogger{sl}, nil
}
