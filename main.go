package main

import (
	"flag"
	"mini_market/container"
	"mini_market/container/srvcontainer"
	"mini_market/controller"
	_ "net/http/pprof"
	"strings"
)

var appCfgPath string

func init() {
	flag.StringVar(&appCfgPath, "c", "", "app configuration file path")
}

// func generateUser(ctn container.Container, n int64) {
// 	dataBuilder, found := dsfactory.GetDataServiceBuilder(config.UserData)
// 	if !found {
// 		panic("cannot found tag data builder")
// 	}
// 	datacfg := ctn.GetConfig().UseCaseCfg.UserSrvCfg.UserDataConfig
// 	iface, err := dataBuilder.(*dsfactory.UserDsFactory).Build(ctn, datacfg, ctn.GetConfig())
// 	if err != nil {
// 		panic(err)
// 	}
// 	srv := iface.(dataservice.UserDataSrv)
// 	var i, id int64
// 	id = srv.GetUsersCount()
// 	for i = 0; i < n; i++ {
// 		id, err = srv.InsertUser(&entity.User{Username: fmt.Sprintf("user_%d@gmail.com", i+1), Password: []byte("123456")})
// 		if err != nil {
// 			panic(err)
// 		}
// 	}
// 	log.Printf("%d user inserted, max id is %d", n, id)
// }

// func generateTags(ctn container.Container, n int64) {
// 	dataBuilder, found := dsfactory.GetDataServiceBuilder(config.TagData)
// 	if !found {
// 		panic("cannot found tag data builder")
// 	}
// 	datacfg := ctn.GetConfig().UseCaseCfg.ItemSrvCfg.TagDataConfig
// 	iface, err := dataBuilder.(*dsfactory.TagDsFactory).Build(ctn, datacfg, ctn.GetConfig())
// 	if err != nil {
// 		panic(err)
// 	}
// 	srv := iface.(*tagdata.TagData)
// 	var (
// 		total int64 = 50000000
// 		per   int64 = 100000
// 		ntag  int64 = 2000
// 		i     int64 = 0
// 	)
// 	wg := &sync.WaitGroup{}
// 	wg.Add(int(total / per))
// 	for ; i < total/per; i++ {
// 		go func(p int64) {
// 			start := p * per
// 			end := (p + 1) * per
// 			for start < end {
// 				srv.AddTagToItem(rand.Int63n(ntag)+1, start)
// 				srv.AddTagToItem(rand.Int63n(ntag)+1, start)
// 				start++
// 			}
// 			wg.Done()
// 		}(i)
// 	}
// 	wg.Wait()
// }

// func generateItems(ctn container.Container, n int64) {
// 	srvBuilder, found := ucfactory.GetUseCaseBuilder(config.ItemSrv)
// 	if !found {
// 		panic("cannot found item service builder")
// 	}
// 	iface, err := srvBuilder.(*ucfactory.ItemSrvFactory).Build(ctn, ctn.GetConfig(), config.ItemSrv)
// 	if err != nil {
// 		panic(err)
// 	}
// 	srv := iface.(*usecases.ItemServicesImpl)
// 	tagCount := srv.TagData.GetTagCount()
// 	var i int64
// 	id := srv.ItemData.GetItemsCount()
// 	wg := &sync.WaitGroup{}
// 	wg.Add(int(n / 1000))
// 	workersCh := make(chan struct{}, 100)
// 	for i := 0; i < 100; i++ {
// 		workersCh <- struct{}{}
// 	}
// 	for i = 0; i < n/1000; i++ {
// 		<-workersCh
// 		go func(j, m int64) {
// 			for ; j < m+j; j++ {
// 				k := atomic.AddInt64(&id, 1)
// 				id, err = srv.InsertItem(&model.Detail{
// 					Name:     fmt.Sprintf("🔥item %d 🤔", k+1),
// 					PhotoUrl: "https://foo.bar/photo.jpg",
// 					Desc:     fmt.Sprintf("This is the item %d", k+1),
// 				}, []int64{rand.Int63n(tagCount) + 1, rand.Int63n(tagCount) + 1})
// 				if err != nil {
// 					panic(err)
// 				}
// 				workersCh <- struct{}{}
// 			}
// 			wg.Done()
// 		}(i, 1000)
// 	}
// 	wg.Wait()
// 	log.Printf("%d tag inserted, max id is %d", n, id)
// }

func spawn_grpc_servers(ctn container.Container) {
	// run servers
	addrs := strings.Split(ctn.GetConfig().ServerCfg.GrpcServerAddr, ",")
	for _, addr := range addrs {
		grpcServer := controller.NewGrpcServer(ctn)
		go func(addr string) {
			grpcServer.Run(addr)
		}(strings.Trim(addr, " "))
	}
}

func main() {
	flag.Parse()
	ctn := srvcontainer.NewServiceContainer()

	// pprof
	// runtime.SetBlockProfileRate(1)
	// go func() {
	// 	log.Println(http.ListenAndServe("localhost:6060", nil))
	// }()

	if appCfgPath == "" {
		panic("need app configuration file")
	}
	err := ctn.InitApp(appCfgPath)
	if err != nil {
		panic(err)
	}

	// prepare data
	// generateUser(ctn, nuser)
	// generateTags(ctn, ntag)
	// generateItems(ctn, nitem)

	spawn_grpc_servers(ctn)

	cfg := ctn.GetConfig().ServerCfg
	ginServer := controller.NewGinServer(cfg.HttpProxyAddr, cfg.GrpcServerAddr)
	ginServer.Run()
}
