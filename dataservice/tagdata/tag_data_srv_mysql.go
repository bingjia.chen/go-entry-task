package tagdata

import (
	"context"
	"errors"
	"fmt"
	"mini_market/config"
	"mini_market/dataservice"
	"mini_market/entity"
	"mini_market/errcode"
	"mini_market/logger"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/patrickmn/go-cache"
	"xorm.io/xorm"
)

type TagData struct {
	engine   *xorm.Engine
	nameToId map[string]int64
	tags     []*entity.Tag
	rdb      *redis.Client

	newChan  chan *entity.Tag
	memcache *cache.Cache
}

var _ dataservice.TagDataSrv = (*TagData)(nil)

const tagKey = "tag:%d"
const tagPageKey = "tp:%d"

func GetTagKey(id int64) string {
	return fmt.Sprintf(tagKey, id)
}

func GetTagPageKey(page int64) string {
	return fmt.Sprintf(tagPageKey, page)
}

func (t *TagData) checkId(id int64) error {
	if id <= 0 {
		return errcode.ErrInvalidArgument
	}
	if id > int64(len(t.tags)) {
		return errcode.ErrNotFound
	}
	return nil
}

func (t *TagData) Init(args map[string]interface{}) error {
	eng, ok := args[config.Mysql].(*xorm.Engine)
	if !ok {
		err := errors.New("tag data service need configuration for sql")
		logger.Log.Errorf(err.Error())
		return err
	}
	t.engine = eng

	rdb, ok := args[config.Redis].(*redis.Client)
	if !ok {
		err := errors.New("tag data service need configuration for redis")
		logger.Log.Errorf(err.Error())
		return err
	}
	t.rdb = rdb

	tags, err := t.GetAllTags()
	if err != nil {
		return err
	}
	t.tags = tags
	t.memcache = cache.New(1*time.Minute, 5*time.Minute)

	t.nameToId = make(map[string]int64)
	for i := 0; i < len(t.tags); i++ {
		t.nameToId[t.tags[i].Name] = t.tags[i].TagId
	}
	t.newChan = make(chan *entity.Tag)
	return nil
}

func (t *TagData) InsertTag(tag *entity.Tag) (int64, error) {
	_, err := t.engine.Insert(tag)
	if err != nil {
		return 0, err
	}
	t.newChan <- tag
	return tag.TagId, nil
}

func (t *TagData) GetTagIDByName(name string) (int64, error) {
	id, found := t.nameToId[name]
	if !found {
		return 0, errcode.ErrNotFound
	}
	return id, nil
}

func (t *TagData) GetTagNameById(id int64) (string, error) {
	err := t.checkId(id)
	if err != nil {
		return "", err
	}
	return t.tags[id-1].Name, nil
}

func (t *TagData) GetAllTags() ([]*entity.Tag, error) {
	tags := make([]*entity.Tag, 0)
	err := t.engine.Select("*").Find(&tags)
	if err != nil {
		return nil, err
	}
	return tags, nil
}

func (t *TagData) GetItemsByTag(tagId, start, pageSize int64) ([]int64, int64, error) {
	err := t.checkId(tagId)
	if err != nil {
		return nil, 0, err
	}
	items, found := t.memcache.Get(GetTagPageKey(start))
	if found {
		ids := items.([]int64)
		return ids[:len(ids)-1], ids[len(ids)-1], nil
	}
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()
	strs, next, err := t.rdb.SScan(ctx, GetTagKey(tagId), uint64(start), "*", pageSize).Result()
	ids := make([]int64, len(strs))
	for i := 0; i < len(strs); i++ {
		ids[i], _ = strconv.ParseInt(strs[i], 10, 64)
	}
	t.memcache.Add(GetTagPageKey(start), append(ids, int64(next)), 0)
	return ids, int64(next), nil
}

func (t *TagData) AddTagToItem(tagId, itemId int64) error {
	err := t.checkId(tagId)
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()
	return t.rdb.SAdd(ctx, GetTagKey(tagId), itemId).Err()
}

func (t *TagData) GetTagCount() int64 {
	return int64(len(t.tags))
}
