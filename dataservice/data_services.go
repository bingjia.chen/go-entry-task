// Package dataservice provide a series of data service for item, user, tags and comments
package dataservice

import (
	"mini_market/container"
	"mini_market/entity"

	"xorm.io/xorm"
)

// UserDataSrv is the data service for user entity
type UserDataSrv interface {
	container.Initializer
	// InsertUser insert an user and return its id
	InsertUser(user *entity.User) (int64, error)
	// GetUserByName return an user's username and return its id.
	GetUserByName(name string) (*entity.User, error)
	// GetUserByID return an user by its id .
	GetUserByID(id int64) (*entity.User, error)
	// GetUsersCount return the user count in database(according to user table's
	// AUTO_INCREMENT id)
	GetUsersCount() int64
}

// ItemDataSrv is the data service for item entity
type ItemDataSrv interface {
	container.Initializer
	container.Finalizer
	// InsertItem insert an item with its detail and return their id (which should
	// be the same)
	InsertItem(item *entity.Item, detail *entity.Detail) (int64, error)
	// GetItemsInPage return items according to ids(acquires that ids are ordered)
	GetItemsInPage(page, pageSize int64) ([]*entity.Item, error)
	GetItemByID(id int64) (*entity.Item, error)
	// GetItemDetailsByID return an item's detail by id
	GetItemDetailsByID(id int64) (*entity.Detail, error)
	// GetItemByName return an item by its name
	GetItemByName(name string) (*entity.Item, error)
	// GetItemsCount return the item count in database
	GetItemsCount() int64
}

type TagDataSrv interface {
	container.Initializer
	// InsertTag insert a tag and return its id
	InsertTag(tag *entity.Tag) (int64, error)
	// GetAllTags return  all the tags in db
	GetAllTags() ([]*entity.Tag, error)
	// GetTagNameById return a tag's by its id
	GetTagNameById(id int64) (string, error)
	// GetTagIDByName return a tag's id by its name
	GetTagIDByName(name string) (int64, error)
	// GetItemsByTag return all the items that is tagged by a tag
	GetItemsByTag(tagId, start, pageSize int64) ([]int64, int64, error)
	// AddTagToItem put a tag on an item
	AddTagToItem(tagId, itemId int64) error
	// GetTagCount return the tag count in database
	GetTagCount() int64
}

type CommentDataSrv interface {
	container.Initializer
	// InsertComment insert a comment and return its id
	InsertComment(comment *entity.Comment) (int64, error)
	// GetCommentsByIds return  comments by ids
	GetCommentsByIds(id []int64) ([]*entity.Comment, error)
	// GetCommentsByItemId return comments that has some itemid
	GetCommentsByItemId(itemId, page, pagesize int64) ([]*entity.Comment, error)
	// GetCommentByReplyTo return comment by its reply
	GetCommentByReplyTo(commentId int64) ([]*entity.Comment, error)
	// GetCommentsCount return the comment count in database
	GetCommentsCount() int64
}

// GetCountOfTable loop up table metadata and return the count in database
func GetCountOfTable(engine *xorm.Engine, tabName string) (int64, error) {
	var nextId int64
	_, err := engine.Table("information_schema.TABLES").
		Select("AUTO_INCREMENT").
		Where("TABLE_SCHEMA = ?", engine.Dialect().URI().DBName).
		And("TABLE_NAME = ?", tabName).
		Get(&nextId)
	if err != nil {
		return 0, err
	}
	return nextId - 1, nil
}
