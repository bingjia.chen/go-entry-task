package dataservice

import "fmt"

type MapFun func(int64) int64

// TableMapper translates an id into table name by rules.
type TableMapper struct {
	ntabs    int64
	pat      string
	tabNames []string
	mf       MapFun
}

func NewTableMapper(ntabs int64, tabPattern string, mf MapFun) *TableMapper {
	tm := &TableMapper{
		ntabs: ntabs,
		pat:   tabPattern,
		mf:    mf,
	}
	tm.tabNames = make([]string, ntabs)
	for i := int64(0); i < ntabs; i++ {
		tm.tabNames[i] = fmt.Sprintf(tm.pat, i)
	}
	return tm
}

func (t *TableMapper) GetTableNameBy(id int64) string {
	if t.mf(id) < 0 || t.mf(id) > int64(len(t.tabNames)) {
		panic(fmt.Sprintf("invalid MapFunc, table count: %d, get %d",
			len(t.tabNames), t.mf(id)))
	}
	return t.tabNames[t.mf(id)]
}

func (t *TableMapper) ForeachTable(applier func(string) (bool, error)) error {
	for _, tab := range t.tabNames {
		stop, err := applier(tab)
		if err != nil {
			return err
		}
		if stop {
			return nil
		}
	}
	return nil
}
