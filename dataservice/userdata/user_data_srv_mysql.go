package userdata

import (
	"context"
	"encoding/json"
	"fmt"
	"mini_market/config"
	"mini_market/dataservice"
	"mini_market/entity"
	"mini_market/logger"
	"sync/atomic"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
	"xorm.io/xorm"
)

type UserData struct {
	engine   *xorm.Engine
	rdb      *redis.Client
	count    int64
	insertCh chan *entity.User
}

var _ dataservice.UserDataSrv = (*UserData)(nil)

var ErrUserNameExist = errors.New("User Name Exist")

const userCountKey = "usercount"
const userKeyPat = "user:%d"
const userNameKey = "user:%s"
const cacheTimeout = time.Millisecond * 500

func GetUserCacheKey(uid int64) string {
	return fmt.Sprintf(userKeyPat, uid)
}

func GetUserNameCacheKey(name string) string {
	return fmt.Sprintf(userNameKey, name)
}

func (i *UserData) saveUserInCache(uid int64, user *entity.User) error {
	jsdata, err := json.Marshal(user)
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	return i.rdb.SetNX(ctx, GetUserCacheKey(uid), jsdata, 0).Err()
}

func (i *UserData) getUserInCache(uid int64) (*entity.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	jsdata, err := i.rdb.Get(ctx, GetUserCacheKey(uid)).Result()
	if err != nil {
		if err == redis.Nil {
			return nil, nil
		}
		return nil, err
	}
	user := &entity.User{}
	err = json.Unmarshal([]byte(jsdata), user)
	if err != nil {
		return nil, err
	}
	user.UserId = uid
	return user, nil
}

func (u *UserData) getCount() (int64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	count, err := u.rdb.Get(ctx, userCountKey).Int64()
	if err == nil {
		u.count = count
		return count, nil
	}
	count, err = dataservice.GetCountOfTable(u.engine, u.engine.TableName(&entity.User{}))
	if err != nil {
		logger.Log.Errorf("cannot get count of user")
		u.count = 0
	}
	ctx, cancel = context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	u.rdb.Set(ctx, userCountKey, count, 0)
	return count, nil
}

func (u *UserData) Init(args map[string]interface{}) error {
	eng, in := args[config.Mysql].(*xorm.Engine)
	if !in {
		err := errors.New("user data service need configuration for sql")
		logger.Log.Errorf(err.Error())
		return err
	}
	u.engine = eng

	rdb, in := args[config.Redis].(*redis.Client)
	if !in {
		err := errors.New("user data service need configuration for redis")
		logger.Log.Errorf(err.Error())
		return err
	}
	u.rdb = rdb
	count, err := u.getCount()
	if err != nil {
		return err
	}
	u.count = count
	u.insertCh = make(chan *entity.User, 1000)
	go u.insertRoutine()
	return nil
}

func (u *UserData) insertRoutine() {
	const maxBuf = 50
	buf := make([]*entity.User, 0, maxBuf)
	ticker := time.NewTicker(time.Second * 5)
	for {
		select {
		case user := <-u.insertCh:
			buf = append(buf, user)
			if len(buf) >= maxBuf {
				_, err := u.engine.Insert(buf)
				if err != nil {
					logger.Log.Errorf("insert user: %v", err)
				}
				buf = []*entity.User{}
			}
		case <-ticker.C:
			if len(buf) > 0 {
				_, err := u.engine.Insert(buf)
				if err != nil {
					logger.Log.Errorf("insert user: %v", err)
				}
				buf = []*entity.User{}
			}
		}
	}
}

func (u *UserData) InsertUser(user *entity.User) (int64, error) {
	id := user.UserId
	atomic.AddInt64(&u.count, 1)
	err := u.saveUserInCache(id, user)
	if err != nil {
		logger.Log.Errorf("saveUserInCache: %v", err)
		return 0, err
	}
	go func() {
		u.insertCh <- user
	}()
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	u.rdb.Incr(ctx, userCountKey)
	return id, nil
}

func (u *UserData) GetUserByName(name string) (*entity.User, error) {
	user := &entity.User{Username: name}
	found, err := u.engine.Get(user)
	if err != nil {
		logger.Log.Errorf(err.Error())
		return nil, err
	}
	if !found {
		return nil, nil
	}
	return user, nil
}

func (u *UserData) GetUserByID(id int64) (*entity.User, error) {
	if val, err := u.getUserInCache(id); err != nil {
		return nil, err
	} else if val != nil {
		return val, err
	}
	user := &entity.User{UserId: id}
	found, err := u.engine.Get(user)
	if err != nil {
		logger.Log.Errorf(err.Error())
		return nil, err
	}
	if !found {
		return nil, nil
	}
	return user, nil
}

func (u *UserData) GetUsersCount() int64 {
	return u.count
}
