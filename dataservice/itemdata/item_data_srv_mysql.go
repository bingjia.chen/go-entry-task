package itemdata

import (
	"bytes"
	"context"
	"encoding/gob"
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"mini_market/config"
	"mini_market/dataservice"
	"mini_market/entity"
	"mini_market/errcode"
	"mini_market/logger"
	"mini_market/utils"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/patrickmn/go-cache"
	"xorm.io/xorm"
)

type ItemData struct {
	engine  *xorm.Engine
	rdb     *redis.Client
	count   int64
	tm      *dataservice.TableMapper
	dm      *dataservice.TableMapper
	bufPool *sync.Pool

	memcache *cache.Cache
	dsp      *utils.Dispatcher
}

const (
	itemCache int64 = iota
	detailCache
	nameCache
)

type cacheMsg struct {
	Typ   int64
	Datas interface{}
}

var _ dataservice.ItemDataSrv = (*ItemData)(nil)

// cache keys
const itemPageKeyPat = "ipage:%d"
const itemKeyPat = "item:%d"
const detailKeyPat = "detail:%d"
const itemNameKeyPat = "iname:%s"
const itemCountKey = "itemcount"

const cacheTimeout = time.Second * 3

func (i *ItemData) getCount() (int64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	count, err := i.rdb.Get(ctx, itemCountKey).Int64()
	if err == nil {
		i.count = count
		return count, nil
	}
	var total int64
	err = i.tm.ForeachTable(func(tabName string) (bool, error) {
		count, err := dataservice.GetCountOfTable(i.engine, tabName)
		if err != nil {
			logger.Log.Panicf("cannot get count of item")
		}
		total += count
		return false, err
	})
	ctx, cancel = context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	i.count = total
	i.rdb.Set(ctx, itemCountKey, total, 0)
	return total, nil
}

func (i *ItemData) Init(args map[string]interface{}) error {
	eng, ok := args[config.Mysql].(*xorm.Engine)
	if !ok {
		err := errors.New("user data service need configuration for sql")
		logger.Log.Errorf(err.Error())
		return err
	}
	i.engine = eng
	rdb, ok := args[config.Redis].(*redis.Client)
	if !ok {
		err := errors.New("redis data service need configuration for redis")
		logger.Log.Errorf(err.Error())
		return err
	}
	i.rdb = rdb
	// mapper for item_tab
	i.tm = dataservice.NewTableMapper(6, "item_tab_%d", func(id int64) int64 {
		return int64(id / 10000000)
	})
	// mapper for detail_tab
	i.dm = dataservice.NewTableMapper(6, "detail_tab_%d", func(id int64) int64 {
		return int64(id / 10000000)
	})

	// buffer pool for gob
	i.bufPool = &sync.Pool{New: func() interface{} {
		return &bytes.Buffer{}
	}}

	i.memcache = cache.New(160*time.Second, 7*time.Minute)

	i.dsp = utils.NewDispatcher(300)
	i.dsp.Run()

	count, err := i.getCount()
	if err != nil {
		return err
	}
	i.count = count
	logger.Log.Infof("item count = %d", i.count)
	return nil
}

func GetItemCacheKey(id int64) string {
	return fmt.Sprintf(itemKeyPat, id)
}

func GetDetailCacheKey(id int64) string {
	return fmt.Sprintf(detailKeyPat, id)
}

func GetItemNameCacheKey(name string) string {
	return fmt.Sprintf(itemNameKeyPat, name)
}

func GetItemPageKey(page int64) string {
	return fmt.Sprintf(itemPageKeyPat, page)
}

func unmarshalDetail(data string) (*entity.Detail, error) {
	d := &entity.Detail{}
	dec := gob.NewDecoder(strings.NewReader(data))
	err := dec.Decode(d)
	if err != nil {
		return nil, err
	}
	return d, nil
}

func unmarshalItem(data string) (*entity.Item, error) {
	i := &entity.Item{}
	dec := gob.NewDecoder(strings.NewReader(data))
	err := dec.Decode(i)
	if err != nil {
		return nil, err
	}
	return i, nil
}

func (i *ItemData) Finalize() error {
	return nil
}

func (i *ItemData) saveInCache(key string, data []byte) error {
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	return i.rdb.Set(ctx, key, data, 0).Err()
}

func (i *ItemData) getInCache(key string) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	jsdata, err := i.rdb.Get(ctx, key).Result()
	if err != nil {
		if err == redis.Nil {
			return "", nil
		}
		return "", err
	}
	return jsdata, nil
}

func (i *ItemData) saveInCachePipeline(kvs map[string][]byte) error {
	pipe := i.rdb.Pipeline()
	defer pipe.Close()
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	for key, val := range kvs {
		pipe.Set(ctx, key, val, 0)
	}
	_, err := pipe.Exec(ctx)
	return err
}

func (i *ItemData) saveItemInCache(id int64, item *entity.Item) error {
	var buf *bytes.Buffer
	buf = i.bufPool.Get().(*bytes.Buffer)
	buf.Reset()
	defer i.bufPool.Put(buf)
	enc := gob.NewEncoder(buf)
	err := enc.Encode(item)
	if err != nil {
		return err
	}
	return i.saveInCache(GetItemCacheKey(id), buf.Bytes())
}

func (i *ItemData) saveItemNameInCache(name string, id int64) error {
	return i.saveInCache(GetItemNameCacheKey(name), []byte(strconv.FormatInt(id, 10)))
}

func (i *ItemData) saveDetailInCache(id int64, detail *entity.Detail) error {
	key := GetDetailCacheKey(id)
	data, err := json.Marshal(detail)
	if err != nil {
		return err
	}
	return i.saveInCache(key, data)
}

func (i *ItemData) GetItemByID(id int64) (*entity.Item, error) {
	// if id <= 0 || id > i.count {
	// 	return nil, errcode.ErrNotFound
	// }

	key := GetItemCacheKey(id)
	if item, found := i.memcache.Get(key); found {
		return item.(*entity.Item), nil
	}

	item, err := i.getItemInRedis(id)
	if err == nil {
		i.memcache.Add(key, item, 0)
		return item, nil
	} else if err != nil && err != redis.Nil {
		return nil, err
	}
	item = &entity.Item{ItemId: id}
	found, err := i.engine.Table(i.tm.GetTableNameBy(id)).Get(item)
	if err != nil {
		return nil, err
	}
	if !found {
		return nil, errcode.ErrNotFound
	}

	// cache items here
	go func() {
		i.dsp.JobQueue <- func() error {
			i.memcache.Add(GetItemCacheKey(id), item, 0)
			itemsMap := make(map[string][]byte)
			nameMap := make(map[string][]byte)
			var buf bytes.Buffer
			enc := gob.NewEncoder(&buf)
			err := enc.Encode(item)
			if err != nil {
				logger.Log.Errorf("%s: %+v", err.Error(), item)
				return err
			}
			itemsMap[GetItemCacheKey(item.ItemId)] = buf.Bytes()
			nameMap[item.Name] = []byte(strconv.FormatInt(item.ItemId, 10))
			if err := i.saveInCachePipeline(itemsMap); err != nil {
				logger.Log.Errorf("fail to save items in redis: %s", err.Error())
				return err
			}
			if err := i.saveInCachePipeline(nameMap); err != nil {
				logger.Log.Errorf("fail to save item names in redis: %s", err.Error())
				return err
			}
			return nil
		}
	}()
	return item, nil
}

func (i *ItemData) getItemsInCache(ids []int64) ([]*entity.Item, error) {
	pipe := i.rdb.Pipeline()
	defer pipe.Close()
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()

	keys := make([]string, len(ids), len(ids))
	for j, id := range ids {
		keys[j] = GetItemCacheKey(id)
	}

	results := make([]*redis.StringCmd, 0, len(ids))
	for _, key := range keys {
		results = append(results, pipe.Get(ctx, key))
	}
	pipe.Exec(ctx)
	// results, err := i.rdb.MGet(ctx, keys...).Result()
	// if err != nil {
	// 	return nil, err
	// }

	items := make([]*entity.Item, 0, len(ids))
	for _, result := range results {
		if result != nil {
			data, err := result.Result()
			if err == nil {
				item, err := unmarshalItem(data)
				if err != nil {
					logger.Log.Errorf("unmarshalItem: %v", err)
					continue
				}
				items = append(items, item)
			}
		}
	}
	return items, nil
}

func (i *ItemData) getItemInRedis(id int64) (*entity.Item, error) {
	data, err := i.getInCache(GetItemCacheKey(id))
	if err != nil {
		return nil, err
	}
	if data == "" {
		return nil, nil
	}
	item, err := unmarshalItem(data)
	if err != nil {
		return nil, err
	}
	item.ItemId = id
	return item, nil
}

func (i *ItemData) getItemIdInCache(name string) (int64, error) {
	idStr, err := i.getInCache(GetItemNameCacheKey(name))
	if err != nil {
		return 0, err
	}
	if idStr == "" {
		return 0, nil
	}
	return strconv.ParseInt(idStr, 10, 64)
}

func (i *ItemData) getDetailInCache(id int64) (*entity.Detail, error) {
	jsdata, err := i.getInCache(GetDetailCacheKey(id))
	if err != nil {
		return nil, err
	}
	if jsdata == "" {
		return nil, nil
	}
	detail, err := unmarshalDetail(jsdata)
	if err != nil {
		return nil, err
	}
	detail.DetailId = id
	return detail, nil
}

func (i *ItemData) GetItemsInPage(page, pageSize int64) ([]*entity.Item, error) {
	start := page * pageSize
	if start == i.count {
		return []*entity.Item{}, nil
	}

	// get items in local memory first
	localPage, found := i.memcache.Get(GetItemPageKey(page))
	if found {
		return localPage.([]*entity.Item), nil
	}

	// now get items in redis
	ids := make([]int64, 0, pageSize)
	for i := start; i < start+20; i++ {
		ids = append(ids, i)
	}
	cachedItems, err := i.getItemsInCache(ids)
	if err != nil {
		logger.Log.Errorf("can't get item in redis: %v", err)
	} else if len(cachedItems) != 0 {
		i.memcache.Set(GetItemPageKey(page), cachedItems, time.Duration(rand.Int31n(300))*time.Second)
		return cachedItems, nil
	}
	// all items are in cached, return
	items := make([]*entity.Item, 0, pageSize)
	// err = i.engine.Table(i.tm.GetTableNameBy(start)).Select("*").Where("item_id >= (SELECT item_id from "+i.tm.GetTableNameBy(start)+" order by item_id asc limit ?, 1)", start).Limit(20).Find(&items)
	err = i.engine.Table(i.tm.GetTableNameBy(start)).Where("item_id between ? and ?", start, start+3*page).Find(&items)
	if err != nil {
		return nil, err
	}
	if len(items) == 0 {
		return nil, nil
	}
	// cache items here
	go func() {
		i.dsp.JobQueue <- func() error {
			for j := int64(0); j < 4; j++ {
				i.memcache.Set(GetItemPageKey(page+j), items[j*pageSize:(j+1)*pageSize], time.Duration(rand.Int31n(120))*time.Second)
			}
			itemsMap := make(map[string][]byte)
			nameMap := make(map[string][]byte)
			for j := 0; j < len(items); j++ {
				var buf bytes.Buffer
				enc := gob.NewEncoder(&buf)
				err := enc.Encode(items[j])
				if err != nil {
					logger.Log.Errorf("%s: %+v", err.Error(), items[j])
					return err
				}
				itemsMap[GetItemCacheKey(items[j].ItemId)] = buf.Bytes()
				nameMap[items[j].Name] = []byte(strconv.FormatInt(items[j].ItemId, 10))
			}
			if err := i.saveInCachePipeline(itemsMap); err != nil {
				logger.Log.Errorf("fail to save items in redis: %s", err.Error())
				return err
			}
			if err := i.saveInCachePipeline(nameMap); err != nil {
				logger.Log.Errorf("fail to save item names in redis: %s", err.Error())
				return err
			}
			return nil
		}
	}()
	return items[:page], nil
}

func (i *ItemData) GetItemDetailsByID(id int64) (*entity.Detail, error) {
	ptr, found := i.memcache.Get(GetDetailCacheKey(id))
	if found {
		return ptr.(*entity.Detail), nil
	}
	detail, err := i.getDetailInCache(id)
	if err != nil {
		logger.Log.Errorf("cannot get Detail in redis: %s", err.Error())
	} else if detail != nil {
		return detail, nil
	}
	detail = &entity.Detail{DetailId: id}
	fmt.Printf("%v\n", detail)
	found, err = i.engine.Table(i.dm.GetTableNameBy(id)).Get(detail)
	if err != nil {
		return nil, err
	}
	if !found {
		return nil, nil
	}
	go func() {
		i.dsp.JobQueue <- func() error {
			key := GetDetailCacheKey(id)
			i.memcache.Add(key, detail, 0)
			buf := i.bufPool.Get().(*bytes.Buffer)
			defer i.bufPool.Put(buf)
			buf.Reset()
			enc := gob.NewEncoder(buf)
			enc.Encode(detail)
			ctx, cancel := context.WithTimeout(context.Background(), time.Second*1)
			defer cancel()
			return i.rdb.Set(ctx, key, buf.Bytes(), 0).Err()
		}
	}()

	return detail, nil
}

func (i *ItemData) GetItemByName(name string) (*entity.Item, error) {
	id, err := i.getItemIdInCache(name)
	if err != nil {
		return nil, err
	}
	if id != 0 {
		item, err := i.getItemInRedis(id)
		if err != nil {
			return nil, err
		}
		if item != nil {
			item.ItemId = id
			return item, nil
		}
	}
	item := &entity.Item{ItemId: 0, Name: name}
	err = i.tm.ForeachTable(func(tab string) (bool, error) {
		found, err := i.engine.Table(tab).Get(item)
		if err != nil {
			return false, err
		}
		return found, nil
	})
	if item.ItemId == 0 {
		return nil, nil
	}
	return item, nil
}

func (i *ItemData) InsertItem(item *entity.Item, detail *entity.Detail) (int64, error) {
	sess := i.engine.NewSession()
	defer sess.Close()
	var err error
	err = sess.Begin()
	if err != nil {
		return 0, err
	}
	_, err = sess.Insert(item)
	if err != nil {
		return 0, err
	}
	detail.DetailId = item.ItemId
	_, err = sess.Insert(detail)
	if err != nil {
		detail.DetailId = 0
		return 0, err
	}
	err = sess.Commit()
	if err != nil {
		return 0, err
	}
	err = i.saveItemInCache(item.ItemId, item)
	if err != nil {
		return 0, err
	}
	err = i.saveDetailInCache(detail.DetailId, detail)
	if err != nil {
		return 0, err
	}
	i.count++
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	i.rdb.Incr(ctx, itemCountKey)
	return item.ItemId, err
}

func (i *ItemData) GetItemsCount() int64 {
	return i.count
}
