package commentdata

import (
	"bytes"
	"context"
	"encoding/gob"
	"errors"
	"fmt"
	"mini_market/config"
	"mini_market/dataservice"
	"mini_market/entity"
	"mini_market/logger"
	"mini_market/utils"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/patrickmn/go-cache"
	"xorm.io/xorm"
)

type CommentData struct {
	engine   *xorm.Engine
	rdb      *redis.Client
	count    int64
	memcache *cache.Cache
	bufPool  *sync.Pool
	dsp      *utils.Dispatcher
}

var _ dataservice.CommentDataSrv = (*CommentData)(nil)

// cache keys
const commentCountKey = "cmtcount"
const commentCacheKey = "cmt:%d"
const commentListCacheKey = "cmtlst:%d"
const replyCacheKey = "cmtrply:%d"

const cacheTimeout time.Duration = time.Second * 1

var errNotSync = errors.New("cache not sync")

func GetCommentCacheKey(commentId int64) string {
	return fmt.Sprintf(commentCacheKey, commentId)
}

func GetCommentListCacheKey(itemId int64) string {
	return fmt.Sprintf(commentListCacheKey, itemId)
}

func GetReplyCacheKey(commentId int64) string {
	return fmt.Sprintf(replyCacheKey, commentId)
}

func (c *CommentData) Init(args map[string]interface{}) error {
	eng, ok := args[config.Mysql].(*xorm.Engine)
	if !ok {
		err := errors.New("user data service need configuration for sql")
		logger.Log.Errorf("%s", err)
		return err
	}
	c.engine = eng
	rdb, ok := args[config.Redis].(*redis.Client)
	if !ok {
		err := errors.New("redis data service need configuration for redis")
		logger.Log.Errorf("%s", err)
		return err
	}
	c.rdb = rdb
	count, err := c.getCount()
	if err != nil {
		return err
	}
	c.count = count
	c.memcache = cache.New(30*time.Second, 3*time.Second)
	c.bufPool = &sync.Pool{New: func() interface{} { return &bytes.Buffer{} }}
	c.dsp = utils.NewDispatcher(500)
	c.dsp.Run()
	go c.syncCount()
	return nil
}

// syncCount write the conut of comments into redis
func (c *CommentData) syncCount() {
	ticker := time.NewTicker(1 * time.Minute)
	for {
		select {
		case <-ticker.C:
			c.dsp.JobQueue <- func() error {
				ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
				defer cancel()
				err := c.rdb.Set(ctx, commentCountKey, strconv.FormatInt(c.count, 10), 0).Err()
				if err != nil {
					logger.Log.Errorf("cannot increase comments count in redis: %s", err)
					return err
				}
				return err
			}
		}

	}
}

// saveCommentInCache use gob to serialize a comment into redis
func (c *CommentData) saveCommentInCache(cmt *entity.Comment) error {
	c.memcache.Set(GetCommentCacheKey(cmt.CommentId), cmt, 0)
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	buf := c.bufPool.Get().(*bytes.Buffer)
	defer func() {
		buf.Reset()
		c.bufPool.Put(buf)
	}()
	enc := gob.NewEncoder(buf)
	enc.Encode(cmt)
	return c.rdb.Set(ctx, GetCommentCacheKey(cmt.CommentId), buf.Bytes(), 0).Err()
}

// saveCommentsInCache use pipeline to save a batch of comment into redis
func (c *CommentData) saveCommentsInCache(cmts []*entity.Comment) error {
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	_, err := c.rdb.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()
		var buf bytes.Buffer
		for _, cmt := range cmts {
			enc := gob.NewEncoder(&buf)
			err := enc.Encode(cmt)
			if err != nil {
				logger.Log.Errorf("cannot save comment[%+v] in redis: %s", cmt, err)
				return err
			}
			return pipe.Set(ctx, GetCommentCacheKey(cmt.CommentId), buf.Bytes(), 0).Err()
		}
		return nil
	})
	return err
}

func (c *CommentData) saveInListCache(key string, ids []int64) error {
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	values := utils.I64ToGeneric(ids)
	return c.rdb.RPush(ctx, key, values...).Err()
}

func (c *CommentData) saveInItemList(itemId int64, ids []int64) error {
	return c.saveInListCache(GetCommentListCacheKey(itemId), ids)
}

func (c *CommentData) saveInReplyList(replyTo int64, ids []int64) error {
	return c.saveInListCache(GetReplyCacheKey(replyTo), ids)
}

func (c *CommentData) GetCommentsByIds(ids []int64) ([]*entity.Comment, error) {
	keys := make([]string, len(ids), len(ids))
	rest := make([]int64, 0, len(ids))
	// get from redis using mget
	for i, id := range ids {
		keys[i] = GetCommentCacheKey(id)
	}
	cmts := make([]*entity.Comment, len(keys), len(keys))

	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	datas, err := c.rdb.MGet(ctx, keys...).Result()
	if err == nil {
		for i := 0; i < len(cmts); i++ {
			if datas[i] == nil {
				rest = append(rest, ids[i])
				continue
			}
			cmts[i] = new(entity.Comment)
			dec := gob.NewDecoder(strings.NewReader(datas[i].(string)))
			err := dec.Decode(cmts[i])
			if err != nil {
				logger.Log.Errorf("cannot get comment[id=%d] in redis: %s", ids[i], err)
				break
			}
		}
		if len(rest) == 0 {
			return cmts, nil
		}
	} else {
		logger.Log.Errorf("cannot get comment in redis: %s", err)
		return nil, nil
	}

	// get comments that not in redis from mysql
	if len(rest) == 0 {
		return cmts, nil
	}
	args := make([]interface{}, len(rest), len(rest))
	restCmts := make([]*entity.Comment, len(rest), len(rest))
	err = c.engine.Table(&entity.Comment{}).In("comment_id", args...).Desc("publish_date").Find(restCmts)
	if err != nil {
		return nil, err
	}
	if len(restCmts) != 0 {
		err = c.saveCommentsInCache(restCmts)
		if err != nil {
			logger.Log.Errorf("cannot save comments: %s", err)
		}
	}
	// now merge the two result set
	for i, j := 0, 0; i < len(cmts); i++ {
		if cmts[i] == nil {
			cmts[i] = restCmts[j]
			j++
		}
	}
	return cmts, nil
}

// InsertComment insert the comment into cache and then store it into mysql
// asynchronously.
func (c *CommentData) InsertComment(comment *entity.Comment) (int64, error) {
	id := atomic.AddInt64(&c.count, 1)
	comment.CommentId = id
	err := c.saveCommentInCache(comment)
	if err != nil {
		logger.Log.Errorf("cannot save comment[%+v] in cache: %s", comment, err)
		return id, err
	}
	c.dsp.JobQueue <- func() error {
		_, err := c.engine.Insert(&entity.Comment{Content: comment.Content, CommentOn: comment.CommentOn, ReplyTo: comment.ReplyTo})
		if err != nil {
			return err
		}
		itemId := comment.CommentOn
		ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
		defer cancel()
		err = c.rdb.LPush(ctx, GetCommentListCacheKey(itemId), id).Err()
		err = c.saveInItemList(itemId, []int64{id})
		if err != nil {
			logger.Log.Errorf("cannot save comment[%+v] in item list: %s", comment, err)
			return err
		}
		if comment.ReplyTo != 0 {
			ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
			defer cancel()
			err = c.rdb.LPush(ctx, GetReplyCacheKey(id), comment.ReplyTo).Err()
			if err != nil {
				logger.Log.Errorf("cannot save comment[%+v] in reply list: %s", comment, err)
			}
		}
		return nil
	}
	return comment.CommentId, nil
}

func (c *CommentData) GetCommentsByItemId(itemId, page, pagesize int64) ([]*entity.Comment, error) {
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	ids, err := c.rdb.LRange(ctx, GetCommentListCacheKey(itemId), 0, pagesize).Result()
	if err == nil && len(ids) != 0 {
		ints, err := utils.AToi64(ids)
		if err != nil {
			logger.Log.Errorf("invalid ids[%+v]: %s", ids, err)
		}
		cmts, err := c.GetCommentsByIds(ints)
		if err == nil {
			return cmts, nil
		}
		c.memcache.Add(GetCommentCacheKey(itemId), cmts, 0)
		logger.Log.Errorf("cannot get comments[ids: %+v]: %s", ints, err)
	} else if err == redis.Nil {
		return nil, nil
	}
	return nil, nil
}

func (c *CommentData) GetCommentByReplyTo(commentId int64) ([]*entity.Comment, error) {
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	datas, err := c.rdb.LRange(ctx, GetReplyCacheKey(commentId), 0, -1).Result()
	ids := make([]int64, 0)
	if err == nil {
		if len(datas) == 0 {
			return nil, nil
		}
		for _, data := range datas {
			id, err := strconv.ParseInt(data, 10, 64)
			if err != nil {
				logger.Log.Errorf("cannot parse comment id[%s]: %s", data, err)
			}
			ids = append(ids, id)
		}
		return c.GetCommentsByIds(ids)
	}

	cmts := make([]*entity.Comment, 0)
	err = c.engine.Desc("publish_date").Find(&cmts, &entity.Comment{ReplyTo: commentId})
	if err != nil {
		return nil, err
	}
	return cmts, nil
}

// getCount should be call at the init function once
func (c *CommentData) getCount() (int64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	count, err := c.rdb.Get(ctx, commentCountKey).Int64()
	if err == nil {
		c.count = count
		return count, nil
	}
	count, err = dataservice.GetCountOfTable(c.engine, (&entity.Comment{}).TableName())
	if err != nil {
		logger.Log.Errorf("cannot get count of comments")
		c.count = 0
	}
	ctx, cancel = context.WithTimeout(context.Background(), cacheTimeout)
	defer cancel()
	c.rdb.Set(ctx, commentCountKey, count, 0)
	return count, nil
}

func (c *CommentData) GetCommentsCount() int64 {
	return c.count
}
