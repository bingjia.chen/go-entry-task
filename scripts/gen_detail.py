from typing import List
import mysql.connector as mysql
import sys

def get_db():
    db = mysql.connect(host="localhost", database="entry_task_db", user="cbj",
                       password="cbj123")
    if not db.is_connected():
        print("db is not connect")
        sys.exit(-1)
    return db

def gen_detail(db: mysql.MySQLConnection, n: int):
    cursor = db.cursor()
    if not cursor:
        print("cannot get cursor")
        return
    stmts: List[str] = []
    for i in range(5):
        stmts.append("insert into `detail_tab_{}` (`desc`, `photo_url`)\nvalues\n".format(i))
    pat: str = "(\"photo_url_{}\", \"This is detail {}\")"
    values: List[str] = []
    per: int = 1000
    cursor.execute("select count(*) from `detail_tab_0`")
    _id = cursor.fetchall()[0][0] + 1
    for _ in range(n // per):
        for _ in range(per):
            values.append(pat.format(_id, _id))
            _id += 1
        try:
            cursor.execute(stmts[_id // 10000000] + (",\n".join(values)))
            values.clear()
        except Exception as e:
            print(e)
            cursor.close()
            sys.exit(-1) 
        db.commit()

    cursor.close()


if __name__ == "__main__":
    gen_detail(get_db(), 50000000)
