from locust import  task, between
from locust.contrib.fasthttp import FastHttpUser
import random, json

path = "/comment?itemid={}&page={}"
token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjQsImV4cCI6MTYyODE2NjIwNCwiaWF0IjoxNjI4MDQ2MjA0LCJpc3MiOiJDQkoifQ.W9L0asQeN9vFWho4utMTg-J5JkhQ6NBIDUCItMfCFkM"


class UserComment(FastHttpUser):
    def on_start(self):
        self.itemid = random.randint(1, 100000)
        self.page = 0

    @task
    def login(self):
        headers = {"Authorization": token}
        with self.client.get(path=path.format(self.itemid, self.page),
                              headers=headers,
                              catch_response=True) as res:
            s = json.loads(res.text)
            if s["errno"] != 0:
                res.failure("error: errno={}".format(s["errno"]))
            else:
                self.itemid = random.randint(1, 100000)

                

    def wait_time(self):
        return 0.5
