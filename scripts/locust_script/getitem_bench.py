from locust import  task, between
from locust.contrib.fasthttp import FastHttpUser
import random, json

path = "/item?page={}"

class GetItemUser(FastHttpUser):
    def on_start(self):
        self.page = 3000
        
    @task
    def get_item(self):
        self.page = (self.page + 1) % (40000000 // 20)
        self.client.request(method="GET", path=path.format(self.page))

    def wait_time(self):
        return 0.2
