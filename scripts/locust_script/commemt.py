from locust import  task, between
from locust.contrib.fasthttp import FastHttpUser
import random, json, threading

path = "/comment"
token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjQsImV4cCI6MTYyODE2NjIwNCwiaWF0IjoxNjI4MDQ2MjA0LCJpc3MiOiJDQkoifQ.W9L0asQeN9vFWho4utMTg-J5JkhQ6NBIDUCItMfCFkM"


class AtomicInteger():
    def __init__(self, value=0):
        self._value = int(value)
        self._lock = threading.Lock()

    def inc(self, d=1):
        with self._lock:
            self._value += int(d)
            return self._value

    def dec(self, d=1):
        return self.inc(-d)

    @property
    def value(self):
        with self._lock:
            return self._value

    @value.setter
    def value(self, v):
        with self._lock:
            self._value = int(v)
            return self._value

item_id = AtomicInteger()

class UserComment(FastHttpUser):
    def on_start(self):
        self.comment_on = item_id.inc()
        self.counter = 0
        self.cmt_id = 0

    @task
    def login(self):
            
        data = {"content": "hello",
                           "comment_on": self.comment_on,
                           "reply_to": 0}
        if self.counter == 4:
            data["reply_to"] = self.cmt_id
            self.comment_on = item_id.inc()
            self.counter = 0

        headers = {"Authorization": token}
        with self.client.post(path=path,
                              headers=headers,
                              data=json.dumps(data), catch_response=True) as res:
            s = json.loads(res.text)
            if s["errno"] != 0:
                res.failure("error: errno={}".format(s["errno"]))
            self.cmt_id = s["comment_id"] 
            self.counter += 1

    def wait_time(self):
        return 0.1
