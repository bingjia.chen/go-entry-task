from locust import  task
from locust.contrib.fasthttp import FastHttpUser
import random, json
import threading
import itertools

path = "/user/register"

class FastWriteCounter(object):
    def __init__(self):
        self._number_of_read = 0
        self._counter = itertools.count()
        self._read_lock = threading.Lock()

    def increment(self):
        next(self._counter)

    def value(self):
        with self._read_lock:
            value = next(self._counter) - self._number_of_read
            self._number_of_read += 1
        return value

_id = FastWriteCounter()

class Register(FastHttpUser):
    def on_start(self):
        self.username = "user_{}@gmail.com"
        self.password = "123456"

    @task
    def register(self):
        i = _id.value()
        _id.increment()
        data = json.dumps({"username": self.username.format(i), "passwd":
                                    self.password})
        self.client.post(path=path, data=data)

    def wait_time(self):
        return 0.3
