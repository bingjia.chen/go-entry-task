from locust import  task, between
from locust.contrib.fasthttp import FastHttpUser
import random, json

path = "/user/login"

class LoginUser(FastHttpUser):
    def on_start(self):
        self.id_ = random.randint(1, 2000)
        self.username = "user_{}@gmail.com"
        self.password = "123456"

    @task
    def login(self):
        data = json.dumps({"username": self.username.format(self.id_), "passwd":
                                    self.password})
        with self.client.post(path=path, data=data, catch_response=True) as res:
            s = json.loads(res.text)
            if s["errno"] != 0:
                res.failure("error: errno={}".format(s["errno"]))

    def wait_time(self):
        return 0.3
