from locust import  task, between
from locust.contrib.fasthttp import FastHttpUser
import random, json

path = "/item/detail?{}"
token = r"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjAsImV4cCI6MTYyODAwMDUzNywiaWF0IjoxNjI3OTk5MzM3LCJpc3MiOiJDQkoifQ.nS6b-oUlTAp7w6ctegf4Dq8r7tqWuZp7z8jUb3izQ_Q"

class SearchItemUser(FastHttpUser):
    def on_start(self):
        self._id = 1

    @task
    def get_item(self):
        args = "itemid={}".format(self._id)
        headers = {"Authorization": token}
        with self.client.request(method="GET", path=path.format(args),
                                 headers=headers,
                                 catch_response=True) as res:
            errno = res.json()["errno"]
            if errno != 0:
                res.failure("errno = {}".format(errno))
            self._id += 1
            
    def wait_time(self):
        return 0.5
