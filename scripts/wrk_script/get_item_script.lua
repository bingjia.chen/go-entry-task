math.randomseed(os.time())

request = function()
  local n = math.random(1000000)
  path = "/item?page=" .. n
  return wrk.format("GET", path)
end
