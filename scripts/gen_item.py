from typing import List
import mysql.connector as mysql
import sys

def get_db():
    db = mysql.connect(host="localhost", database="entry_task_db", user="cbj",
                       password="cbj123")
    if not db.is_connected():
        print("db is not connect")
        sys.exit(-1)
    return db

def gen_item(db: mysql.MySQLConnection, n: int):
    cursor = db.cursor()
    if not cursor:
        print("cannot get cursor")
        return
    stmts: List[str] = []
    for i in range(5):
        stmts.append("insert into `item_tab_{}` (name) values".format(i))
    pat: str = "(\"item_{}\")"
    values: List[str] = []
    per: int = 1000
    cursor.execute("select count(*) from `item_tab_0`")
    _id = cursor.fetchall()[0][0] + 1
    for _ in range(n // per):
        for _ in range(per):
            values.append(pat.format(_id))
            _id += 1
        try:
            cursor.execute(stmts[_id // 10000000] + (",\n".join(values)))
            values.clear()
        except Exception as e:
            print(e)
            cursor.close()
            sys.exit(-1) 
        db.commit()

    cursor.close()


if __name__ == "__main__":
    gen_item(get_db(), 50000000)
