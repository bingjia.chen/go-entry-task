// Code generated by "stringer -type=ErrCode -linecomment"; DO NOT EDIT.

package errcode

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[ErrOK-0]
	_ = x[ErrNotFound-1]
	_ = x[ErrPermissionDenied-2]
	_ = x[ErrAlreadyExists-3]
	_ = x[ErrUnknown-4]
	_ = x[ErrInvalidToken-5]
	_ = x[ErrTokenExpired-6]
	_ = x[ErrInvalidArgument-7]
	_ = x[ErrInternal-8]
}

const _ErrCode_name = "OKNot FoundPermission DeniedAlready ExistsUnknown ErrorInvalid TokenInvalid TokenInvalid ArgumentInternal Error"

var _ErrCode_index = [...]uint8{0, 2, 11, 28, 42, 55, 68, 81, 97, 111}

func (i ErrCode) String() string {
	if i < 0 || i >= ErrCode(len(_ErrCode_index)-1) {
		return "ErrCode(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _ErrCode_name[_ErrCode_index[i]:_ErrCode_index[i+1]]
}
