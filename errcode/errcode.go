package errcode

//go:generate stringer -type=ErrCode -linecomment
type ErrCode int

const (
	ErrOK               ErrCode = iota // OK
	ErrNotFound                        // Not Found
	ErrPermissionDenied                // Permission Denied
	ErrAlreadyExists                   // Already Exists
	ErrUnknown                         // Unknown Error
	ErrInvalidToken                    // Invalid Token
	ErrTokenExpired                    // Invalid Token
	ErrInvalidArgument                 // Invalid Argument
	ErrInternal                        // Internal Error
)

func (e ErrCode) Error() string {
	return e.String()
}
