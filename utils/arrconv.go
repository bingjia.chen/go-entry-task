package utils

import "strconv"

func AToi64(s []string) ([]int64, error) {
	var (
		a   int64
		err error
		is  []int64
	)
	l := len(s)
	is = make([]int64, l, l)
	for i := 0; i < l; i++ {
		a, err = strconv.ParseInt(s[i], 10, 64)
		if err != nil {
			return is, err
		}
		is[i] = a
	}
	return is, nil
}

func I64ToGeneric(is []int64) []interface{} {
	arr := make([]interface{}, len(is), len(is))
	for i, is := range is {
		arr[i] = is
	}
	return arr
}
