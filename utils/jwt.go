package utils

import (
	"mini_market/errcode"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
)

type Claim struct {
	UserId int64 `json:"uid"`
	jwt.StandardClaims
}

var Issuer string = "CBJ"
var JWTExpireDuration time.Duration = 0
var JWTSignKey []byte = []byte("")

func defaultClaim() jwt.StandardClaims {
	now := time.Now()
	return jwt.StandardClaims{
		ExpiresAt: now.Add(JWTExpireDuration).Unix(),
		IssuedAt:  now.Unix(),
		Issuer:    Issuer,
	}
}

func GenToken(uid int64) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &Claim{uid, defaultClaim()})
	ss, err := token.SignedString(JWTSignKey)
	if err != nil {
		return "", err
	}
	return ss, nil
}

func ParseToken(tok string) (*Claim, error) {
	token, err := jwt.ParseWithClaims(tok, &Claim{}, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.Wrap(errcode.ErrInvalidToken, "Wrong Signing Method")
		}
		return JWTSignKey, nil
	})
	if err != nil {
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&(jwt.ValidationErrorExpired) != 0 {
				return nil, errcode.ErrTokenExpired
			}
		}
		return nil, errcode.ErrInvalidToken
	}
	clms, ok := token.Claims.(*Claim)
	if ok && token.Valid {
		return clms, nil
	}
	return nil, errcode.ErrInvalidToken
}
