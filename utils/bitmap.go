package utils

import (
	"fmt"
	"sync"
)

const MaxBitMapSize uint64 = 1 << 20
const pageShift uint64 = 6
const pageSize uint64 = 1 << 6

type Bitmap struct {
	bits *[]uint64
	mu   *sync.Mutex
}

func NewBitmap(bits *[]uint64) *Bitmap {
	return &Bitmap{
		bits: bits,
		mu:   &sync.Mutex{},
	}
}

func (b *Bitmap) Get(i uint64) (int, bool) {
	b.mu.Lock()
	defer b.mu.Unlock()
	if i >= b.Size() {
		return 0, false
	}
	page := (*b.bits)[(i >> pageShift)]
	offset := (i & (pageSize - 1))
	return int((page & (1 << offset)) >> offset), true
}

func (b *Bitmap) Set(i, value uint64, grow bool) bool {
	b.mu.Lock()
	defer b.mu.Unlock()
	if i >= b.Size() {
		if !grow {
			return false
		}
		// grow size
		sz := (i >> pageShift) + 1
		if sz > MaxBitMapSize {
			panic(fmt.Sprintf("bitmap reach maxwidth i = %d value = %d", i, value))
		}
		newBits := make([]uint64, sz)
		copy(newBits, *b.bits)
		b.bits = &newBits
	}
	offset := (i & (pageSize - 1))
	if value == 0 {
		(*b.bits)[(i >> pageShift)] &= ^(1 << offset)
	} else {
		(*b.bits)[(i >> pageShift)] |= (1 << offset)
	}
	return true
}

func (b *Bitmap) Size() uint64 {
	return uint64(len(*b.bits) << pageShift)
}

func (b *Bitmap) FieldSet(start, count uint64) []int64 {
	b.mu.Lock()
	defer b.mu.Unlock()
	if start < 0 {
		panic("out of range")
	}
	var p, page, startPage, offset, idx uint64
	startPage = start >> pageShift
	idxes := make([]int64, 0, count)
	offset = start & (pageSize - 1)
	for p = startPage; p < uint64(len(*b.bits)) && count > 0; p++ {
		page = (*b.bits)[p] >> offset
		idx = (p << pageShift) + offset
		for page != 0 && count > 0 {
			if page&0x1 == 1 {
				idxes = append(idxes, int64(idx)+1)
				count--
			}
			page >>= 1
			idx += 1
		}
		offset = 0
	}
	return idxes
}
