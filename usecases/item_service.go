package usecases

import (
	"math"
	"mini_market/dataservice"
	"mini_market/entity"
	"mini_market/errcode"
	"mini_market/logger"
	"mini_market/model"
	"sync"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

type ItemServicesImpl struct {
	ItemData    dataservice.ItemDataSrv
	TagData     dataservice.TagDataSrv
	CountInPage int64
	IdArrayPool *sync.Pool
}

var _ ItemServices = (*ItemServicesImpl)(nil)

func (i *ItemServicesImpl) Init(args map[string]interface{}) error {
	i.IdArrayPool = &sync.Pool{
		New: func() interface{} {
			return make([]int64, i.CountInPage, i.CountInPage)
		},
	}
	return nil
}

func (i *ItemServicesImpl) validateItemId(id int64) error {
	if id <= 0 {
		return errcode.ErrInvalidArgument
	}
	if id > i.ItemData.GetItemsCount() {
		return errcode.ErrNotFound
	}
	return nil
}

func (i *ItemServicesImpl) validatePage(page int64) error {
	if page < 0 {
		return errcode.ErrInvalidArgument
	}
	if float64(page) >= math.Ceil(float64(i.ItemData.GetItemsCount())/float64(i.CountInPage)) {
		return errcode.ErrNotFound
	}
	return nil
}

func (i *ItemServicesImpl) validateDetail(d *model.Detail) error {
	return validation.ValidateStruct(d,
		validation.Field(&d.Name, validation.Required, validation.Length(1, 32)),
		validation.Field(&d.PhotoUrl, is.URL, validation.Length(1, 2083)))
}

func (i *ItemServicesImpl) itemToModel(item *entity.Item) *model.Item {
	return &model.Item{
		ItemId: item.ItemId,
		Name:   item.Name,
	}
}

func (i *ItemServicesImpl) InsertItem(detail *model.Detail, tags []int64) (int64, error) {
	err := i.validateDetail(detail)
	if err != nil {
		logger.Log.Debugf("item[%+v] InsertItem fail: %s", detail, err)
		return 0, errcode.ErrInvalidArgument
	}
	id, err := i.ItemData.InsertItem(&entity.Item{Name: detail.Name}, &entity.Detail{PhotoUrl: detail.PhotoUrl, Desc: detail.Desc})
	if err != nil {
		logger.Log.Errorf("item[%+v] InsertItem fail: %s", detail, err)
		return 0, err
	}
	for _, tagId := range tags {
		if err := i.TagData.AddTagToItem(tagId, id); err != nil {
			logger.Log.Errorf("item[%+v] InsertItem fail: %s", detail, err)
			return 0, err
		}
	}
	return id, nil
}

func (i *ItemServicesImpl) GetItems(page int64) ([]*model.Item, error) {
	err := i.validatePage(page)
	if err != nil {
		logger.Log.Errorf("item[page=%d, count=%d] GetItems fail: %s", page, i.ItemData.GetItemsCount(), err)
		return nil, err
	}
	items, err := i.ItemData.GetItemsInPage(page, i.CountInPage)
	if err != nil {
		logger.Log.Errorf("item[page=%d] GetItems fail: %s", page, err)
		return nil, err
	}
	result := make([]*model.Item, 0, len(items))
	for j := int64(0); j < int64(len(items)); j++ {
		result = append(result, i.itemToModel(items[j]))
	}
	return result, nil
}

func (i *ItemServicesImpl) GetItemByName(name string) (*model.Item, error) {
	if l := len(name); l == 0 || l > 32 {
		return nil, errcode.ErrInvalidArgument
	}
	item, err := i.ItemData.GetItemByName(name)
	if err != nil {
		logger.Log.Errorf("item[name=%s] register fail: %s", name, err)
		return nil, err
	}
	if item == nil {
		return nil, nil
	}
	if item == nil {
		panic(item)
	}
	return i.itemToModel(item), nil
}

func (i *ItemServicesImpl) GetDetailById(id int64) (*model.Detail, error) {
	var (
		item   *entity.Item
		detail *entity.Detail
		err    error
	)
	err = i.validateItemId(id)
	if err != nil {
		logger.Log.Errorf("item[id=%d] GetDetailById fail: %s", id, err)
		return nil, err
	}
	item, err = i.ItemData.GetItemByID(id)
	if err != nil {
		logger.Log.Debugf("item[id=%d] GetDetailById fail: %s", id, err)
		item = &entity.Item{}
	}
	if item == nil {
		logger.Log.Debugf("item[id=%d] GetDetailById fail: item not found")
		item = &entity.Item{}
	}
	detail, err = i.ItemData.GetItemDetailsByID(item.ItemId)
	if err != nil {
		logger.Log.Errorf("item[id=%d] GetDetailById fail: %s", id, err)
		return nil, err
	}
	return &model.Detail{
		ItemId:   item.ItemId,
		Name:     item.Name,
		PhotoUrl: detail.PhotoUrl,
		Desc:     detail.Desc,
	}, nil
}

func (i *ItemServicesImpl) GetCountInPage() int64 {
	return i.CountInPage
}

func (i *ItemServicesImpl) GetItemsByTags(tagName string, lastId int64) ([]*model.Item, int64, error) {
	startId := lastId + 1
	err := i.validateItemId(startId)
	if err != nil {
		logger.Log.Debugf("tag[name=%s, lastId=%d] GetItemsByTag fail: %s", tagName, lastId, err)
		return nil, 0, err
	}
	tagId, err := i.TagData.GetTagIDByName(tagName)
	if err != nil {
		logger.Log.Errorf("tag[name=%s, lastId=%d] GetItemsByTag fail: %s", tagName, lastId, err)
		return nil, 0, err
	}
	ids, next, err := i.TagData.GetItemsByTag(tagId, startId, i.CountInPage)
	if err != nil {
		logger.Log.Errorf("tag[name=%s, lastId=%d] GetItemsByTag fail: %s", tagName, lastId, err)
		return nil, 0, err
	}
	items := make([]*entity.Item, 0, i.CountInPage)
	for _, id := range ids {
		item, err := i.ItemData.GetItemByID(id)
		if err != nil {
			logger.Log.Errorf("tag[name=%s, lastId=%d] GetItemsByTag fail: %s", tagName, lastId, err)
			return nil, 0, err
		}
		items = append(items, item)
	}
	result := make([]*model.Item, 0, len(items))
	for j := 0; j < len(items); j++ {
		if items[j] != nil {
			result = append(result, i.itemToModel(items[j]))
		}
	}
	return result, next, nil
}
