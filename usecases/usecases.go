package usecases

import (
	"mini_market/container"
	"mini_market/model"
)

type UserServices interface {
	Register(user *model.User) (token string, err error)
	Login(user *model.User) (token string, err error)
	CheckAuth(token string) (uid int64, err error)
}

type ItemServices interface {
	container.Initializer
	// GetItemsByIDs return items according to ids.
	// NOTE: ids must be ascending
	GetItems(page int64) ([]*model.Item, error)
	GetItemByName(name string) (*model.Item, error)
	GetDetailById(id int64) (*model.Detail, error)
	GetCountInPage() int64

	GetItemsByTags(tagName string, lastId int64) ([]*model.Item, int64, error)
	InsertItem(detail *model.Detail, tags []int64) (int64, error)
}

type CommentService interface {
	GetCommentOfItem(itemId, page int64) ([]*model.Comment, error)
	AddComment(uid, replyTo, commentOn int64, content string) (int64, error)
	GetReplyOfComment(cmtId int64) ([]*model.Comment, error)
}
