package usecases

import (
	"bytes"
	"mini_market/dataservice"
	"mini_market/entity"
	"mini_market/errcode"
	"mini_market/logger"
	"mini_market/model"
	"mini_market/utils"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-sql-driver/mysql"
)

type UserServicesImpl struct {
	UserData dataservice.UserDataSrv
}

var _ (UserServices) = (*UserServicesImpl)(nil)

const PasswordCost = 8

func (u *UserServicesImpl) validateUser(user *model.User) error {
	return validation.ValidateStruct(user,
		validation.Field(&user.Username, validation.Required, validation.Length(5, 32)),
		validation.Field(&user.Password, validation.Required, validation.Length(5, 32)),
	)
}

func (u *UserServicesImpl) toEntity(md *model.User) *entity.User {
	return &entity.User{
		Username: md.Username,
		Password: md.Password,
	}
}

func (u *UserServicesImpl) validateUserId(uid int64) error {
	if uid < 0 {
		return errcode.ErrInvalidArgument
	}
	if uid > u.UserData.GetUsersCount() {
		return errcode.ErrNotFound
	}
	return nil
}

func (u *UserServicesImpl) Register(user *model.User) (string, error) {
	err := u.validateUser(user)
	if err != nil {
		logger.Log.Debugf("user[%+v] register fail: %s", err)
		return "", errcode.ErrInvalidArgument
	}
	// hash, err := bcrypt.GenerateFromPassword(user.Password, PasswordCost)
	// if err != nil {
	// 	logger.Log.Errorf("user[%+v] register fail: %s", err)
	// 	return "", errcode.ErrUnknown
	// }
	// user.Password = hash
	// user.Password = user.Password
	id, err := u.UserData.InsertUser(u.toEntity(user))
	if err != nil {
		switch err.(type) {
		case *mysql.MySQLError:
			e := err.(*mysql.MySQLError)
			if e.Number == uint16(1062) {
				return "", errcode.ErrAlreadyExists
			}
		default:
			logger.Log.Errorf("user[%+v] register fail: %s", err)
			return "", errcode.ErrUnknown
		}
	}
	user.Uid = id
	return utils.GenToken(id)
}

func (u *UserServicesImpl) Login(user *model.User) (string, error) {
	err := u.validateUser(user)
	if err != nil {
		logger.Log.Debugf("user[%+v] login fail: %s", err)
		return "", errcode.ErrInvalidArgument
	}
	data, err := u.UserData.GetUserByName(user.Username)
	if err != nil {
		logger.Log.Debugf("user[%+v] login fail: %s", err)
		return "", errcode.ErrInternal
	}
	if data == nil {
		return "", errcode.ErrNotFound
	}
	if bytes.Compare(data.Password, user.Password) != 0 {
		return "", errcode.ErrNotFound
	}
	// err = bcrypt.CompareHashAndPassword(data.Password, []byte(user.Password))
	// if err != nil {
	// 	logger.Log.Debugf("user[%+v] login fail: %s", err)
	// 	return "", errcode.ErrPermissionDenied
	// }
	user.Uid = data.UserId
	return utils.GenToken(data.UserId)
}

func (u *UserServicesImpl) CheckAuth(token string) (int64, error) {
	claim, err := utils.ParseToken(token)
	if err != nil {
		logger.Log.Debugf("user check auth fail: %+v", err)
		return 0, err
	}
	return claim.UserId, nil
}
