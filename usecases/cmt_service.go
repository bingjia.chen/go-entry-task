package usecases

import (
	"mini_market/dataservice"
	"mini_market/entity"
	"mini_market/errcode"
	"mini_market/logger"
	"mini_market/model"

	validation "github.com/go-ozzo/ozzo-validation"
)

type CommentServiceImpl struct {
	CommentData dataservice.CommentDataSrv
	ItemData    dataservice.ItemDataSrv
	UserData    dataservice.UserDataSrv
	CountInPage int64
}

var _ CommentService = (*CommentServiceImpl)(nil)

func (c *CommentServiceImpl) validateComment(cmt *entity.Comment) error {
	return validation.ValidateStruct(cmt,
		validation.Field(&cmt.Content, validation.Required, validation.Length(1, 1024)),
		validation.Field(&cmt.CommentOn, validation.Required, validation.Max(c.ItemData.GetItemsCount()), validation.Min(1)),
		validation.Field(&cmt.PublisherId, validation.Required, validation.Max(c.UserData.GetUsersCount()), validation.Min(1)),
		validation.Field(&cmt.ReplyTo, validation.Max(c.CommentData.GetCommentsCount()), validation.Min(0)),
	)
}

func (c *CommentServiceImpl) toModel(cmt *entity.Comment) *model.Comment {
	md := &model.Comment{}
	md.Cid = cmt.CommentId
	md.Content = cmt.Content
	user, err := c.UserData.GetUserByID(cmt.PublisherId)
	if err != nil || user == nil {
		md.PbName = "None"
		md.Content = ""
		md.ReplyTo = 0
	}
	md.PbName = user.Username
	md.ReplyTo = cmt.ReplyTo
	md.PublishDate = cmt.PublishDate
	return md
}

func (c *CommentServiceImpl) GetCommentOfItem(itemId, page int64) ([]*model.Comment, error) {
	if itemId <= 0 || page < 0 || page*c.CountInPage >= c.CommentData.GetCommentsCount() {
		logger.Log.Debugf("Comment[ItemId=%d, page=%d] GetCommentOfItem: %s", itemId, page, errcode.ErrInvalidArgument)
		return nil, errcode.ErrInvalidArgument
	}
	if itemId > c.ItemData.GetItemsCount() {
		return nil, errcode.ErrNotFound
	}
	cmts, err := c.CommentData.GetCommentsByItemId(itemId, page, 5)
	if err != nil {
		logger.Log.Errorf("Comment[ItemId=%d, page=%d] GetCommentOfItem: %s", itemId, page, err)
		if code, ok := err.(errcode.ErrCode); ok {
			return nil, code
		}
		return nil, errcode.ErrInternal
	}
	results := make([]*model.Comment, len(cmts), len(cmts))
	for i := 0; i < len(cmts); i++ {
		results[i] = c.toModel(cmts[i])
	}
	return results, nil
}

func (c *CommentServiceImpl) AddComment(uid, replyTo, commentOn int64, content string) (int64, error) {
	cmt := &entity.Comment{
		Content:     content,
		PublisherId: uid,
		ReplyTo:     replyTo,
		CommentOn:   commentOn,
	}
	if uid == 0 {
		logger.Log.Errorf("Comment[%+v] GetCommentOfItem: uid is zero")
		return 0, errcode.ErrPermissionDenied
	}
	if err := c.validateComment(cmt); err != nil {
		logger.Log.Errorf("Comment[%+v] GetCommentOfItem: %s", cmt, err)
		return 0, errcode.ErrInvalidArgument
	}
	id, err := c.CommentData.InsertComment(cmt)
	if err != nil {
		logger.Log.Errorf("Comment[%+v] GetCommentOfItem: %v", cmt, err)
		if code, ok := err.(errcode.ErrCode); ok {
			return 0, code
		}
		return 0, errcode.ErrInternal
	}
	return id, nil
}

func (c *CommentServiceImpl) GetReplyOfComment(cmtId int64) ([]*model.Comment, error) {
	if cmtId <= 0 {
		return nil, errcode.ErrInvalidArgument
	}
	if cmtId > c.CommentData.GetCommentsCount() {
		return nil, errcode.ErrNotFound
	}
	cmts, err := c.CommentData.GetCommentByReplyTo(cmtId)
	if err != nil {
		logger.Log.Errorf("Comment[cmtId=%d] GetCommentOfItem: %s", cmtId, err)
		if code, ok := err.(errcode.ErrCode); ok {
			return nil, code
		}
		return nil, errcode.ErrInternal
	}
	results := make([]*model.Comment, len(cmts), len(cmts))
	for i := 0; i < len(cmts); i++ {
		results[i] = c.toModel(cmts[i])
	}
	return results, nil
}
