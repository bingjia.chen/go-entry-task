package ucfactory

import (
	"errors"
	"mini_market/config"
	"mini_market/container"
	"mini_market/container/dsfactory"
	"mini_market/dataservice/itemdata"
	"mini_market/dataservice/tagdata"
	"mini_market/usecases"
)

type ItemSrvFactory struct {
}

var _ (UseCaseBuilder) = (*ItemSrvFactory)(nil)

func (i *ItemSrvFactory) Build(ctn container.Container, ac *config.AppConfig, key string) (UseCaseBuilderIface, error) {
	val, found := ctn.Get(key)
	if found {
		return val, nil
	}

	ucCfg := ac.UseCaseCfg.ItemSrvCfg
	itemDataBuilder, found := dsfactory.GetDataServiceBuilder(ucCfg.ItemDataConfig.Code)
	if !found {
		return nil, errors.New("data service for item not found")
	}
	itemDataSrv, err := itemDataBuilder.Build(ctn, ucCfg.ItemDataConfig, ac)
	if err != nil {
		return nil, err
	}
	tagDataBuilder, found := dsfactory.GetDataServiceBuilder(ucCfg.TagDataConfig.Code)
	if !found {
		return nil, errors.New("data service for item not found")
	}
	tagDataSrv, err := tagDataBuilder.Build(ctn, ucCfg.TagDataConfig, ac)
	if err != nil {
		return nil, err
	}

	var uc usecases.ItemServices
	uc = &usecases.ItemServicesImpl{
		ItemData:    itemDataSrv.(*itemdata.ItemData),
		TagData:     tagDataSrv.(*tagdata.TagData),
		CountInPage: ucCfg.CountInPage,
	}
	if initer, ok := uc.(container.Initializer); ok {
		if err := initer.Init(nil); err != nil {
			return nil, err
		}
	}
	return uc, nil
}
