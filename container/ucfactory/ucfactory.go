// package ucfactory define a series of usecase factory.
// You can define how a usecase is built by implement UseCaseBuilder.
package ucfactory

import (
	"mini_market/config"
	"mini_market/container"
)

var UseCaseFactoryMap = map[string]UseCaseBuilder{
	config.UserSrv: &UserSrvFactory{},
	config.ItemSrv: &ItemSrvFactory{},
	config.CmtSrv:  &CmtSrvFactory{},
}

type UseCaseBuilderIface interface{}

type UseCaseBuilder interface {
	Build(ctn container.Container, ac *config.AppConfig, key string) (UseCaseBuilderIface, error)
}

func GetUseCaseBuilder(key string) (UseCaseBuilder, bool) {
	val, found := UseCaseFactoryMap[key]
	return val, found
}
