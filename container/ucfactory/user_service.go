package ucfactory

import (
	"errors"
	"mini_market/config"
	"mini_market/container"
	"mini_market/container/dsfactory"
	"mini_market/dataservice/userdata"
	"mini_market/usecases"
	"mini_market/utils"
	"time"
)

type UserSrvFactory struct {
}

var _ (UseCaseBuilder) = (*UserSrvFactory)(nil)

func (u *UserSrvFactory) Build(ctn container.Container, ac *config.AppConfig, key string) (UseCaseBuilderIface, error) {
	val, found := ctn.Get(key)
	if found {
		return val, nil
	}

	ucCfg := ac.UseCaseCfg.UserSrvCfg
	userDataBuilder, found := dsfactory.GetDataServiceBuilder(ucCfg.UserDataConfig.Code)
	if !found {
		return nil, errors.New("data service for user not found")
	}
	dataSrv, err := userDataBuilder.Build(ctn, ucCfg.UserDataConfig, ac)
	if err != nil {
		return nil, err
	}
	uc := &usecases.UserServicesImpl{}
	uc.UserData = dataSrv.(*userdata.UserData)
	utils.JWTExpireDuration = time.Duration(ucCfg.JWTExpireSec) * time.Second
	utils.JWTSignKey = []byte(ucCfg.JWTSigningKey)
	return uc, nil
}
