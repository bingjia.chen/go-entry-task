package ucfactory

import (
	"errors"
	"mini_market/config"
	"mini_market/container"
	"mini_market/container/dsfactory"
	"mini_market/dataservice/commentdata"
	"mini_market/dataservice/itemdata"
	"mini_market/dataservice/userdata"
	"mini_market/usecases"
)

type CmtSrvFactory struct {
}

var _ (UseCaseBuilder) = (*CmtSrvFactory)(nil)

func (i *CmtSrvFactory) Build(ctn container.Container, ac *config.AppConfig, key string) (UseCaseBuilderIface, error) {
	val, found := ctn.Get(key)
	if found {
		return val, nil
	}

	ucCfg := ac.UseCaseCfg.CommentSrv
	itemDataBuilder, found := dsfactory.GetDataServiceBuilder(ucCfg.ItemDataConfig.Code)
	if !found {
		return nil, errors.New("data service for item not found")
	}
	itemDataSrv, err := itemDataBuilder.Build(ctn, ucCfg.ItemDataConfig, ac)
	if err != nil {
		return nil, err
	}

	userDataBuilder, found := dsfactory.GetDataServiceBuilder(ucCfg.UserDataConfig.Code)
	if !found {
		return nil, errors.New("data service for user not found")
	}
	userDataSrv, err := userDataBuilder.Build(ctn, ucCfg.UserDataConfig, ac)
	if err != nil {
		return nil, err
	}

	cmtDataBuilder, found := dsfactory.GetDataServiceBuilder(ucCfg.CmtDataConfig.Code)
	if !found {
		return nil, errors.New("data service for comment not found")
	}
	cmtDataSrv, err := cmtDataBuilder.Build(ctn, ucCfg.CmtDataConfig, ac)
	if err != nil {
		return nil, err
	}

	var uc usecases.CommentService
	uc = &usecases.CommentServiceImpl{
		ItemData:    itemDataSrv.(*itemdata.ItemData),
		UserData:    userDataSrv.(*userdata.UserData),
		CommentData: cmtDataSrv.(*commentdata.CommentData),
		CountInPage: ucCfg.CountInPage,
	}
	if initer, ok := uc.(container.Initializer); ok {
		if err := initer.Init(nil); err != nil {
			return nil, err
		}
	}
	return uc, nil
}
