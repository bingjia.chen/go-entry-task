package dsfactory

import (
	"mini_market/config"
	"mini_market/container"
	"mini_market/dataservice"
	"mini_market/dataservice/itemdata"
	"mini_market/logger"
)

type ItemDsFactory struct{}

var _ DataSrvFactory = (*ItemDsFactory)(nil)

func (i *ItemDsFactory) Build(ctn container.Container, cfg *config.DataConfig, appCfg *config.AppConfig) (DataSrvIface, error) {
	if val, found := ctn.Get(cfg.Code); found {
		return val, nil
	}
	datasrcs, err := parseDataSrcCfg(cfg.DataSourceCode, ctn, appCfg)
	if err != nil {
		logger.Log.Panicf("fail to build data service %s: %w", cfg.Code, err)
	}
	var srv dataservice.ItemDataSrv
	srv = &itemdata.ItemData{}
	if initer, ok := srv.(container.Initializer); ok {
		initer.Init(datasrcs)
	}
	ctn.Put(cfg.Code, srv)
	return srv, nil
}
