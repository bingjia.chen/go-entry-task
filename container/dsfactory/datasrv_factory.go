// package dsfactory  defines a series of data service factory
package dsfactory

import (
	"mini_market/container"
	"mini_market/container/strgfactory"
	"strings"

	"mini_market/config"

	"github.com/pkg/errors"
)

// dsFactoryMap holds all the factory
var dsFactoryMap = map[string]DataSrvFactory{
	config.UserData: &UserDsFactory{},
	config.ItemData: &ItemDsFactory{},
	config.TagData:  &TagDsFactory{},
	config.CmtData:  &CmtDsFactory{},
}

type DataSrvIface interface {
}

type DataSrvFactory interface {
	Build(ctn container.Container, cfg *config.DataConfig, appCfg *config.AppConfig) (DataSrvIface, error)
}

// GetDataServiceBuilder return a DataService builder by its code
func GetDataServiceBuilder(code string) (DataSrvFactory, bool) {
	val, found := dsFactoryMap[code]
	return val, found
}

// parseDataSrcCfg parse DataConfig into an argument map
func parseDataSrcCfg(dataSrcCode string, ctn container.Container, appCfg *config.AppConfig) (map[string]interface{}, error) {
	srcCodes := strings.Split(dataSrcCode, " ")
	datasrcs := make(map[string]interface{}, len(srcCodes))
	for _, code := range srcCodes {
		dataSrcBuilder, found := strgfactory.GetDataStoreFactory(code)
		if !found {
			return nil, errors.Errorf("data source %s not found", code)
		}
		dataSrv, err := dataSrcBuilder.Build(ctn, appCfg)
		if err != nil {
			return nil, err
		}
		datasrcs[code] = dataSrv
	}
	return datasrcs, nil
}
