package dsfactory

import (
	"mini_market/config"
	"mini_market/container"
	"mini_market/dataservice"
	"mini_market/dataservice/commentdata"
	"mini_market/logger"
)

type CmtDsFactory struct{}

var _ DataSrvFactory = (*CmtDsFactory)(nil)

func (c *CmtDsFactory) Build(ctn container.Container, cfg *config.DataConfig, appCfg *config.AppConfig) (DataSrvIface, error) {
	if val, found := ctn.Get(cfg.Code); found {
		return val, nil
	}
	datasrcs, err := parseDataSrcCfg(cfg.DataSourceCode, ctn, appCfg)
	if err != nil {
		logger.Log.Panicf("fail to build data service %s: %w", cfg.Code, err)
	}
	var srv dataservice.CommentDataSrv
	srv = &commentdata.CommentData{}
	if initer, ok := srv.(container.Initializer); ok {
		initer.Init(datasrcs)
	}
	ctn.Put(cfg.Code, srv)
	return srv, nil
}
