package strgfactory

import (
	"mini_market/config"
	"mini_market/container"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"xorm.io/xorm"
	"xorm.io/xorm/caches"
	"xorm.io/xorm/log"
)

type MysqlFactory struct {
}

// Build build a database instance(redis, mysql) for storing data
var _ StrgBuilder = (*MysqlFactory)(nil)

func (m *MysqlFactory) Build(ctn container.Container, cfg *config.AppConfig) (DataStoreIface, error) {
	mysqlCfg := cfg.MysqlCfg
	logCfg := cfg.LogCfg
	if value, found := ctn.Get(config.Mysql); found {
		engine := value.(*xorm.Engine)
		return engine, nil
	}
	engine, err := xorm.NewEngine(mysqlCfg.DriverName, mysqlCfg.UrlAddr)
	if err != nil {
		return nil, err
	}
	if mysqlCfg.LRUCacherEnabled {
		cacher := caches.NewLRUCacher(caches.NewMemoryStore(), 1<<11)
		engine.SetDefaultCacher(cacher)
	}
	switch logCfg.Level {
	case config.Debug:
		engine.SetLogLevel(log.LOG_DEBUG)
	case config.Info:
		engine.SetLogLevel(log.LOG_INFO)
	case config.Error:
		engine.SetLogLevel(log.LOG_ERR)
	case config.Warn:
		engine.SetLogLevel(log.LOG_WARNING)
	case config.Panic:
		engine.SetLogLevel(log.LOG_UNKNOWN)
	}
	engine.SetMaxOpenConns(mysqlCfg.MaxOpenConn)
	engine.SetMaxIdleConns(mysqlCfg.MaxIdleConn)
	engine.SetConnMaxLifetime(time.Duration(mysqlCfg.ConnMaxLifetimeSec) * time.Second)
	ctn.Put(config.Mysql, engine)
	return engine, nil
}
