// package strgfactory provides factory for storage handlers(mysql, redis)
package strgfactory

import (
	"mini_market/config"
	"mini_market/container"
)

var strgFactoryMap = map[string]StrgBuilder{
	config.Mysql: &MysqlFactory{},
	config.Redis: &RedisFactory{},
}

type DataStoreIface interface{}

type StrgBuilder interface {
	// Build build a database instance(redis, mysql) for storing data
	// NOTE: cfg must be pointer
	Build(ctn container.Container, cfg *config.AppConfig) (DataStoreIface, error)
}

// GetDataStoreFactory return a database instance by key
func GetDataStoreFactory(key string) (StrgBuilder, bool) {
	val, found := strgFactoryMap[key]
	return val, found
}
