package strgfactory

import (
	"context"
	"mini_market/config"
	"mini_market/container"
	"time"

	"github.com/go-redis/redis/v8"
)

type RedisFactory struct {
}

// Build build a database instance(redis, mysql) for storing data
var _ StrgBuilder = (*RedisFactory)(nil)

func (r *RedisFactory) Build(ctn container.Container, cfg *config.AppConfig) (DataStoreIface, error) {
	redisCfg := cfg.RedisCfg
	if value, found := ctn.Get(config.Redis); found {
		rdb := value.(*redis.Client)
		return rdb, nil
	}

	rdb := redis.NewClient(&redis.Options{
		Addr:               redisCfg.UrlAddr,
		Password:           redisCfg.Passwd,
		DB:                 redisCfg.Db,
		PoolSize:           redisCfg.PoolSize,
		MinIdleConns:       redisCfg.MinIdleConns,
		IdleTimeout:        time.Duration(redisCfg.IdleTimeoutSec) * time.Second,
		MaxConnAge:         time.Duration(redisCfg.MaxConnAgeSec) * time.Second,
		IdleCheckFrequency: time.Duration(redisCfg.IdleCheckFreqSec) * time.Second,
	})
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()
	if err := rdb.Ping(ctx).Err(); err != nil {
		panic(err)
	}

	ctn.Put(config.Redis, rdb)
	return rdb, nil
}
