// package container provides a simple dependency injection interface to create specific
// beans according to configuration files
package container

import "mini_market/config"

type Container interface {
	// InitApp initializes the container.
	// It reads the config file provided, creates an AppConfig,
	// and builds some instances
	InitApp(filename string) error

	// Get returns a cached instance by code
	// NOTE: DO NOT use it to build a bean
	Get(code string) (interface{}, bool)

	// Put caches an instance with its code provided in config file
	Put(code string, value interface{})

	// GetConfig return an AppConfig parsed from a yaml file
	GetConfig() *config.AppConfig

	// Finalize do some cleanup for all Finalizer
	// NOTE: Do not call other bean's Finalize method in a Finalize()
	Finalize() error
}

type Initializer interface {
	// Init will be called when the bean is created
	Init(args map[string]interface{}) error
}

type Finalizer interface {
	// Finalize will be called before the container is finalized
	Finalize() error
}
