package logfactory

import (
	"mini_market/config"
	"mini_market/logger"
)

type ZapLogFactory struct{}

var _ logFactory = (*ZapLogFactory)(nil)

func (z *ZapLogFactory) Build(cfg *config.AppConfig) error {
	l, err := logger.NewZapLogger(cfg.LogCfg)
	if err != nil {
		return err
	}
	logger.SetLogger(l)
	return nil
}
