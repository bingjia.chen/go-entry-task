package logfactory

import (
	"mini_market/config"
)

var loggerFactoryMap = map[string]logFactory{
	config.Zap:        &ZapLogFactory{},
	config.DefaultLog: &DefaultLogFactory{},
}

type logFactory interface {
	Build(cfg *config.AppConfig) error
}

// GetLogFactory return a log builder by its code
func GetLogFactory(key string) (logFactory, bool) {
	val, found := loggerFactoryMap[key]
	return val, found
}
