package logfactory

import (
	"mini_market/config"
	"mini_market/logger"
)

type DefaultLogFactory struct{}

var _ logFactory = (*DefaultLogFactory)(nil)

func (d *DefaultLogFactory) Build(cfg *config.AppConfig) error {
	l := logger.NewDefaultLogger(cfg)
	logger.SetLogger(l)
	return nil
}
