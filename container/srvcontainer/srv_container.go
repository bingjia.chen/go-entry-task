// srvcontainer implement a simple Container
package srvcontainer

import (
	"mini_market/config"
	"mini_market/container"
	"mini_market/container/logfactory"
)

type ServiceContainer struct {
	BeanMap map[string]interface{}
	AppCfg  *config.AppConfig
}

// loadConfig return an AppConfig parsed with a yaml file
func loadConfig(filename string) (*config.AppConfig, error) {
	ac, err := config.ReadConfig(filename)
	if err != nil {
		return nil, err
	}
	return ac, nil
}

// loadLogger load the logger by LogConfig
func loadLogger(ac *config.AppConfig) error {
	if ac.LogCfg == nil {
		builder, _ := logfactory.GetLogFactory(config.DefaultLog)
		return builder.Build(ac)
	}
	builder, found := logfactory.GetLogFactory(ac.LogCfg.Code)
	if !found {
		panic("unknown log factory")
	}
	return builder.Build(ac)
}

// NewServiceContainer return a ServiceContainer
// NOTE: call InitApp() before use it
func NewServiceContainer() *ServiceContainer {
	ctn := &ServiceContainer{}
	ctn.BeanMap = make(map[string]interface{})
	return ctn
}

// GetConfig return the global app config
func (s *ServiceContainer) GetConfig() *config.AppConfig {
	return s.AppCfg
}

func (s *ServiceContainer) InitApp(filename string) error {
	ac, err := loadConfig(filename)
	if err != nil {
		return err
	}
	err = loadLogger(ac)
	if err != nil {
		return err
	}
	s.AppCfg = ac
	s.BeanMap = make(map[string]interface{})
	return nil
}

func (s *ServiceContainer) Finalize() error {
	for _, bean := range s.BeanMap {
		if f, ok := bean.(container.Finalizer); ok {
			if err := f.Finalize(); err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *ServiceContainer) Get(code string) (interface{}, bool) {
	value, found := s.BeanMap[code]
	return value, found
}

func (s *ServiceContainer) Put(code string, value interface{}) {
	s.BeanMap[code] = value
}
