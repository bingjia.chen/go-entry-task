package config

import (
	"context"
	"log"
	"os"
	"regexp"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
)

// Config is an interface that can be Validated
type Config interface {
	Validate(app *AppConfig) error
}

var _ Config = (*MysqlConfig)(nil)
var _ Config = (*RedisConfig)(nil)
var _ Config = (*LogConfig)(nil)
var _ Config = (*ServerConfig)(nil)
var _ Config = (*UseCaseConfig)(nil)
var _ Config = (*UserSrvConfig)(nil)

var ErrInvalidCode error = errors.New("Invalid Code")

func validateConfig(appCfg *AppConfig) error {
	var err error
	if appCfg.MysqlCfg == nil || appCfg.MysqlCfg.Validate(appCfg) != nil {
		return err
	}
	if appCfg.RedisCfg == nil || appCfg.RedisCfg.Validate(appCfg) != nil {
		return err
	}
	if appCfg.ServerCfg == nil || appCfg.ServerCfg.Validate(appCfg) != nil {
		return err
	}
	if appCfg.LogCfg != nil && appCfg.LogCfg.Validate(appCfg) != nil {
		return err
	}
	if appCfg.UseCaseCfg != nil && appCfg.UseCaseCfg.Validate(appCfg) != nil {
		return err
	}
	return nil
}

func (l LogConfig) Validate(ac *AppConfig) error {
	var errMsg string
	errMsg = "ZapConfig: "
	if l.Level != Debug &&
		l.Level != Warn &&
		l.Level != Info &&
		l.Level != Error &&
		l.Level != Panic {
		return errors.New(errMsg + "invalid log level")
	}
	if l.Code != Zap &&
		l.Code != DefaultLog {
		return errors.Wrap(ErrInvalidCode, "LogConfig")
	}
	if l.Code == DefaultLog {
		return nil
	}
	if l.ConfigFileName == "" {
		return nil
	}
	if matched, err := regexp.Match("*.json", []byte(l.ConfigFileName)); err != nil {
		return errors.Wrap(err, "LogConfig")
	} else if !matched {
		return errors.New(errMsg + "config file must be json")
	}
	_, err := os.Stat(l.ConfigFileName)
	if os.IsNotExist(err) {
		return errors.New(errMsg + "config file not found")
	}
	if os.IsPermission(err) {
		return errors.New(errMsg + "config file permission denied")
	}
	return nil
}

func (s ServerConfig) Validate(ac *AppConfig) error {
	var errMsg string
	errMsg = "ServerConfig: "
	if s.GrpcServerAddr == "" {
		errMsg += "grpcServerAddr not found"
	} else if s.HttpProxyAddr == "" {
		errMsg += "httpServerAddr not found"
	} else {
		return nil
	}
	return errors.New(errMsg)
}

func (m MysqlConfig) Validate(ac *AppConfig) error {
	errMsg := "MysqlConfig: "

	// check driver name
	drv := ac.MysqlCfg.DriverName
	if drv != "mysql" {
		return errors.New(errMsg + "invalid driver name")
	}
	return nil
}

func (r RedisConfig) Validate(ac *AppConfig) error {
	errMsg := "RedisConfig: "
	client := redis.NewClient(&redis.Options{
		Addr:     r.UrlAddr,
		Password: r.Passwd,
		DB:       r.Db,
	})
	ctx, cancel := context.WithTimeout(context.Background(), 500*time.Millisecond)
	defer cancel()
	if _, err := client.Ping(ctx).Result(); err != nil {
		return errors.New(errMsg + "cannot ping redis")
	}
	return nil
}

func (u UseCaseConfig) Validate(ac *AppConfig) error {
	if u.UserSrvCfg != nil {
		if err := u.UserSrvCfg.Validate(ac); err != nil {
			return errors.Wrap(err, "user service config")
		}
	} else {
		log.Println("user service disabled")
	}
	if u.ItemSrvCfg != nil {
		if err := u.ItemSrvCfg.Validate(ac); err != nil {
			return errors.Wrap(err, "item service config")
		}
	} else {
		log.Println("user service disabled")
	}
	return nil
}

func (u UserSrvConfig) Validate(ac *AppConfig) error {
	cfg := ac.UseCaseCfg.UserSrvCfg
	// errMsg := "UserSrvConfig: "
	if cfg.Code != UserSrv {
		return errors.Wrap(ErrInvalidCode, "UserSrvConfig")
	}
	if cfg.JWTExpireSec <= 0 {
		return errors.New("jwt expire min must greater than 0")
	}
	return nil
}

func (i ItemSrvConfig) Validate(ac *AppConfig) error {
	cfg := ac.UseCaseCfg.ItemSrvCfg
	// errMsg := "UserSrvConfig: "
	if cfg.Code != ItemSrv {
		return errors.Wrap(ErrInvalidCode, "ItemSrvConfig")
	}
	if cfg.CountInPage <= 0 {
		return errors.New("CountInPage must be larger than 0")
	}
	return nil
}

func (c CommentSrvConfig) Validate(ac *AppConfig) error {
	cfg := ac.UseCaseCfg.CommentSrv
	// errMsg := "UserSrvConfig: "
	if cfg.Code != CmtSrv {
		return errors.Wrap(ErrInvalidCode, "ItemSrvConfig")
	}
	return nil
}
