package config

type UseCaseConfig struct {
	UserSrvCfg *UserSrvConfig    `yaml:"usersrv"`
	CommentSrv *CommentSrvConfig `yaml:"cmtsrv"`
	ItemSrvCfg *ItemSrvConfig    `yaml:"itemsrv"`
}

type UserSrvConfig struct {
	Code           string      `yaml:"code"`
	UserDataConfig *DataConfig `yaml:"userData"`
	JWTSigningKey  string      `yaml:"jwtSignKey"`
	JWTExpireSec   int         `yaml:"jwtExpireSec"`
}

type ItemSrvConfig struct {
	Code            string      `yaml:"code"`
	ItemDataConfig  *DataConfig `yaml:"itemData"`
	TagDataConfig   *DataConfig `yaml:"tagData"`
	CountInPage     int64       `yaml:"countInPage"`
	TagWriteBackSec int64       `yaml:"tagWriteBackSec"`
}

type CommentSrvConfig struct {
	Code           string      `yaml:"code"`
	ItemDataConfig *DataConfig `yaml:"itemData"`
	UserDataConfig *DataConfig `yaml:"userData"`
	CmtDataConfig  *DataConfig `yaml:"cmtData"`
	CountInPage    int64       `yaml:"countInPage"`
}
