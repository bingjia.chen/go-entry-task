// Package config define a series of configuration that can be use to parse from
// yaml file
package config

import (
	"io/ioutil"

	"github.com/pkg/errors"
	yaml "gopkg.in/yaml.v2"
)

// Log levels
const (
	Debug string = "debug"
	Info  string = "info"
	Warn  string = "warn"
	Error string = "error"
	Panic string = "panic"
)

// Data service code
const (
	UserData string = "userData"
	ItemData string = "itemData"
	CmtData  string = "cmtData"
	TagData  string = "tagData"
)

// Use case code
const (
	UserSrv string = "usersrv"
	ItemSrv string = "itemsrv"
	CmtSrv  string = "cmtsrv"
)

// Codes for configuration
const (
	// Databases
	Mysql string = "mysql"
	Redis string = "redis"

	// Log component
	Zap        string = "zap"
	DefaultLog string = "default"
)

// AppConfig saves and maintains all the config in config file
type AppConfig struct {
	MysqlCfg   *MysqlConfig   `yaml:"mysqlConfig"`
	RedisCfg   *RedisConfig   `yaml:"redisConfig"`
	LogCfg     *LogConfig     `yaml:"logConfig"`
	ServerCfg  *ServerConfig  `yaml:"serverConfig"`
	UseCaseCfg *UseCaseConfig `yaml:"useCaseConfig"`
}

type ServerConfig struct {
	GrpcServerAddr string `yaml:"grpcServerAddr"`
	HttpProxyAddr  string `yaml:"httpProxyAddr"`
	MaxConnIdleSec int    `yaml:"maxConnIdleSec"`
	ConnTimeoutSec int    `yaml:"connTimeOutSec"`
}

type MysqlConfig struct {
	UrlAddr            string `yaml:"urlAddr"`
	DriverName         string `yaml:"driverName"`
	LRUCacherEnabled   bool   `yaml:"lruCacherEnabled"`
	MaxOpenConn        int    `yaml:"maxOpenConn"`
	MaxIdleConn        int    `yaml:"maxIdleConn"`
	ConnMaxLifetimeSec int    `yaml:"connMaxLifetimeSec"`
}

type RedisConfig struct {
	UrlAddr          string `yaml:"urlAddr"`
	Passwd           string `yaml:"password"`
	Db               int    `yaml:"db"`
	PoolSize         int    `yaml:"poolSize"`
	MinIdleConns     int    `yaml:"MinIdleConns"`
	MaxConnAgeSec    int    `yaml:"MaxConnAgeSec"`
	IdleTimeoutSec   int    `yaml:"IdleTimeoutSec"`
	IdleCheckFreqSec int    `yaml:"IdleCheckFreqSec"`
}

type LogConfig struct {
	Code           string `yaml:"code"`
	Level          string `yaml:"level"`
	EnableCaller   bool   `yaml:"enableCaller"`
	ConfigFileName string `yaml:"configFileName"`
}

type DataConfig struct {
	Code           string `yaml:"code"`
	DataSourceCode string `yaml:"dataSource"`
}

// ReadConfig read configurations from yaml file and parse it
func ReadConfig(filename string) (*AppConfig, error) {
	var ac *AppConfig
	ac = new(AppConfig)
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(file, ac)
	if err != nil {
		return nil, err
	}

	err = validateConfig(ac)
	if err != nil {
		return nil, errors.Wrap(err, "invalid config")
	}
	return ac, nil
}
