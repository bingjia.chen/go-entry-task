// Package test provides ...
package test

import (
	"context"
	"fmt"
	"io"
	"mini_market/config"
	"mini_market/container"
	"mini_market/container/srvcontainer"
	"mini_market/container/strgfactory"
	"mini_market/container/ucfactory"
	"mini_market/controller"
	"mini_market/entity"
	"mini_market/errcode"
	"mini_market/model"
	"mini_market/usecases"
	"strconv"
	"syscall"
	"testing"

	cpb "mini_market/controller/protobuf/comment"
	ipb "mini_market/controller/protobuf/item"
	upb "mini_market/controller/protobuf/user"

	"github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/suite"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"xorm.io/xorm"
)

type ServerTestSuite struct {
	suite.Suite
	server *controller.GrpcServer
	ctn    container.Container
	addr   string

	token string
	user  *model.User
	db    *xorm.Engine
	rdb   *redis.Client
}

func (s *ServerTestSuite) SetupSuite() {
	s.ctn = srvcontainer.NewServiceContainer()
	if err := s.ctn.InitApp("./config.yaml"); err != nil {
		panic(err)
	}
	cfg := s.ctn.GetConfig().ServerCfg
	s.addr = cfg.GrpcServerAddr
	s.server = controller.NewGrpcServer(s.ctn)
	go s.server.Run("0.0.0.0:5050")
}

func (s *ServerTestSuite) KillServer() {
	syscall.Kill(syscall.Getpid(), syscall.SIGINT)
}

func (s *ServerTestSuite) TearDownSuite() {

	// _, err := s.db.Exec("truncate item")
	// if err != nil {
	// 	s.T().Log(err)
	// }

	// _, err = s.db.Exec("truncate detail")
	// if err != nil {
	// 	s.T().Log(err)
	// }
	// err = s.rdb.FlushAll(context.Background()).Err()
	// if err != nil {
	// 	s.T().Log(err)
	// }
	s.KillServer()
}

func (s *ServerTestSuite) getErrcode(trailer metadata.MD) errcode.ErrCode {
	data := trailer.Get("errcode")
	s.True(len(data) != 0)
	code, err := strconv.Atoi(data[0])
	s.NoError(err, "invalid errcode")
	return errcode.ErrCode(code)
}

func (s *ServerTestSuite) TestAUserService() {
	conn, err := grpc.Dial(s.addr, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	client := upb.NewUserSrvClient(conn)
	var md metadata.MD
	// test invalid argument
	_, err = client.Register(context.Background(), &upb.LoginRequest{Username: "", Passwd: ""}, grpc.Trailer(&md))
	s.NoError(err, "rpc return err")
	code := s.getErrcode(md)
	s.Equal(errcode.ErrInvalidArgument, code)

	reply, err := client.Register(context.Background(), &upb.LoginRequest{Username: "username", Passwd: "passwd"}, grpc.Trailer(&md))
	s.NoError(err, "rpc return err")
	code = s.getErrcode(md)
	s.Equal(errcode.ErrOK, code)
	s.NotEmpty(reply.Token)

	reply, err = client.Login(context.Background(), &upb.LoginRequest{Username: "username", Passwd: "passwd"}, grpc.Trailer(&md))
	s.NoError(err, "rpc return err")
	code = s.getErrcode(md)
	s.Equal(errcode.ErrOK, code)
	s.NotEmpty(reply.Token)
	s.token = reply.Token

	dbBuilder, found := strgfactory.GetDataStoreFactory(config.Mysql)
	if !found {
		panic("SQLdb not find")
	}
	db, err := dbBuilder.Build(s.ctn, s.ctn.GetConfig())
	if err != nil {
		panic(err)
	}
	s.db = db.(*xorm.Engine)

	rdbBuilder, found := strgfactory.GetDataStoreFactory(config.Redis)
	if !found {
		panic("redis not find")
	}
	db, err = rdbBuilder.Build(s.ctn, s.ctn.GetConfig())
	if err != nil {
		panic(err)
	}
	s.rdb = db.(*redis.Client)
}

func (s *ServerTestSuite) prepareItemdata(nitem int) {
	builder, found := ucfactory.GetUseCaseBuilder(config.ItemSrv)
	s.True(found, "item server bean not found")

	srv, err := builder.Build(s.ctn, s.ctn.GetConfig(), config.ItemSrv)
	s.NoError(err)
	itemsrv := srv.(*usecases.ItemServicesImpl)
	items := make([]*model.Item, nitem)

	for i := 0; i < 10; i++ {
		itemsrv.TagData.InsertTag(&entity.Tag{Name: fmt.Sprintf("%d", i)})
	}

	if itemsrv.ItemData.GetItemsCount() == 0 {
		for i := 0; i < nitem; i++ {
			items[i] = &model.Item{Name: fmt.Sprintf("%d", i)}
			id, err := itemsrv.InsertItem(&model.Detail{
				Name:     items[i].Name,
				PhotoUrl: "http://foo.com/baf.jpg",
				Desc:     "This is description",
			}, []int64{int64(i%10) + 1})
			s.NoError(err)
			items[i].ItemId = id
		}
	}
}

func (s *ServerTestSuite) TestBGetItems() {

	nitem := 10000
	s.prepareItemdata(nitem)
	conn, err := grpc.Dial(s.addr, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	client := ipb.NewItemSrvClient(conn)

	for p := 0; p < nitem/20; p++ {
		stream, err := client.GetItems(context.Background(), &ipb.ItemRequest{Page: int64(p)})
		s.NoError(err)
		j := int64(p*20 + 1)
		for {
			item, err := stream.Recv()
			if err == io.EOF {
				break
			}
			s.NoError(err)
			s.Equal(item.ItemId, j, "item id not match")
			j++
		}
		ec := s.getErrcode(stream.Trailer())
		s.Equal(errcode.ErrOK, ec)
	}
}

func (s *ServerTestSuite) TestCItemSearch() {
	nitem := 10000
	s.prepareItemdata(nitem)

	conn, err := grpc.Dial(s.addr, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	client := ipb.NewItemSrvClient(conn)

	// send with no token
	stream, err := client.Search(context.Background(),
		&ipb.SearchRequest{Key: fmt.Sprintf("%d", 0), Type: ipb.SearchRequest_ByTag, Payload: 0})
	_, err = stream.Recv()
	s.Equal(err, io.EOF)
	ec := s.getErrcode(stream.Trailer())
	s.Equal(errcode.ErrPermissionDenied, ec)

	var j, lastId int64
	for t := 0; t < 10; t++ {
		lastId = 0
		for p := 0; p < nitem/10/20; p++ {
			ctx := metadata.AppendToOutgoingContext(context.Background(), "token", s.token)
			stream, err := client.Search(ctx,
				&ipb.SearchRequest{Key: fmt.Sprintf("%d", t), Type: ipb.SearchRequest_ByTag, Payload: lastId})
			s.NoError(err)
			j = int64(p*20 + 1)
			for {
				item, err := stream.Recv()
				if err == io.EOF {
					break
				}
				s.NoError(err)
				s.Equal((item.ItemId-1)%10, int64(t), "item id not match")
				j++
				lastId = item.ItemId
			}
			ec := s.getErrcode(stream.Trailer())
			s.Equal(errcode.ErrOK, ec)
		}
	}
}

func (s *ServerTestSuite) TestDItemDetail() {
	nitem := 10000
	s.prepareItemdata(nitem)

	conn, err := grpc.Dial(s.addr, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	client := ipb.NewItemSrvClient(conn)

	for i := 0; i < nitem; i++ {
		var md metadata.MD
		reply, err := client.GetDetail(metadata.NewOutgoingContext(context.Background(), metadata.Pairs("token", s.token)),
			&ipb.DetailRequest{ItemId: int64(i) + 1}, grpc.Trailer(&md))
		s.NoError(err)
		s.Equal(errcode.ErrOK, s.getErrcode(md))
		s.Equal(fmt.Sprintf("%d", int64(i)), reply.Name)
		s.Equal("http://foo.com/baf.jpg", reply.PhotoUrl)
		s.Equal("This is description", reply.Desc)
	}
}

func (s *ServerTestSuite) TestEAddComment() {
	nitem := 10000
	s.prepareItemdata(nitem)

	conn, err := grpc.Dial(s.addr, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	client := cpb.NewCmtSrvClient(conn)

	var md metadata.MD
	for i := 0; i < nitem; i++ {
		reply, err := client.AddComment(
			metadata.NewOutgoingContext(context.Background(), metadata.Pairs("token", s.token)),
			&cpb.AddRequest{
				Content:   fmt.Sprintf("this is content of item %d", int64(i)+1),
				ReplyTo:   0,
				CommentOn: int64(i) + 1,
			}, grpc.Trailer(&md))
		s.NoError(err)
		s.NotEqual(int64(0), reply.Id)
		s.Equal(errcode.ErrOK, s.getErrcode(md))
	}

}

func (s *ServerTestSuite) TestFGetComment() {
	nitem := 10000
	s.prepareItemdata(nitem)

	conn, err := grpc.Dial(s.addr, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	client := cpb.NewCmtSrvClient(conn)
	for i := 0; i < nitem; i++ {
		stream, err := client.GetComments(
			metadata.NewOutgoingContext(context.Background(), metadata.Pairs("token", s.token)),
			&cpb.CommentRequest{Id: int64(i) + 1, Page: 0},
		)
		s.NoError(err)
		for {
			cmt, err := stream.Recv()
			if err == io.EOF {
				break
			}
			s.NoError(err)
			s.Equal(cmt.Content, fmt.Sprintf("this is content of item %d", int64(i)+1))
		}
		md := stream.Trailer()
		s.Equal(errcode.ErrOK, s.getErrcode(md))
	}
}

func (s *ServerTestSuite) TestGGetReply() {
	nitem := 10000
	s.prepareItemdata(nitem)

	conn, err := grpc.Dial(s.addr, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	client := cpb.NewCmtSrvClient(conn)

	var md metadata.MD
	// first 10 comment is not reply
	for i := 0; i < 10; i++ {
		reply, err := client.AddComment(
			metadata.NewOutgoingContext(context.Background(), metadata.Pairs("token", s.token)),
			&cpb.AddRequest{
				Content:   fmt.Sprintf("this is content of item %d", int64(i)+1),
				ReplyTo:   0,
				CommentOn: int64(i) + 1,
			}, grpc.Trailer(&md))
		s.NoError(err)
		s.Equal(errcode.ErrOK, s.getErrcode(md))
		s.NotEqual(int64(0), reply.Id)
	}

	for i := 10; i < nitem; i++ {
		reply, err := client.AddComment(
			metadata.NewOutgoingContext(context.Background(), metadata.Pairs("token", s.token)),
			&cpb.AddRequest{
				Content:   fmt.Sprintf("this is content of item %d", int64(i)+1),
				ReplyTo:   int64(i%10) + 1,
				CommentOn: int64(i%10) + 1,
			}, grpc.Trailer(&md))
		s.NoError(err)
		s.Equal(errcode.ErrOK, s.getErrcode(md))
		s.NotEqual(int64(0), reply.Id)
	}

	for i := 0; i < nitem/1000; i++ {
		stream, err := client.GetReplyOfComment(
			metadata.NewOutgoingContext(context.Background(), metadata.Pairs("token", s.token)),
			&cpb.CommentRequest{Id: int64(i) + 1, Page: 0},
		)
		s.NoError(err)
		count := 0
		for {
			_, err := stream.Recv()
			if err == io.EOF {
				break
			}
			s.NoError(err)
			count++
		}
		s.Equal((nitem-10)/10, count)
		md := stream.Trailer()
		s.Equal(errcode.ErrOK, s.getErrcode(md))
	}
}

func TestGrpcServer(t *testing.T) {
	suite.Run(t, new(ServerTestSuite))
}
