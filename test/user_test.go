package test

import (
	"context"
	"encoding/json"
	"mini_market/config"
	"mini_market/container"
	"mini_market/container/srvcontainer"
	"mini_market/container/strgfactory"
	"mini_market/container/ucfactory"
	"mini_market/dataservice/userdata"
	"mini_market/entity"
	"mini_market/errcode"
	"mini_market/model"
	"mini_market/usecases"
	"mini_market/utils"
	"testing"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/suite"
	"xorm.io/xorm"
)

type UserTestSuite struct {
	suite.Suite
	UserId   int64
	Username string
	Passwd   []byte
	Token    string
	ctn      container.Container
	service  usecases.UserServices
	db       *xorm.Engine
	rdb      *redis.Client
}

func (u *UserTestSuite) SetupSuite() {
	u.ctn = srvcontainer.NewServiceContainer()
	if err := u.ctn.InitApp("./config.yaml"); err != nil {
		panic(err)
	}

	dbBuilder, found := strgfactory.GetDataStoreFactory(config.Mysql)
	if !found {
		panic("SQLdb not find")
	}
	db, err := dbBuilder.Build(u.ctn, u.ctn.GetConfig())
	if err != nil {
		panic(err)
	}
	u.db = db.(*xorm.Engine)

	rdbBuilder, found := strgfactory.GetDataStoreFactory(config.Redis)
	if !found {
		panic("redis not find")
	}
	db, err = rdbBuilder.Build(u.ctn, u.ctn.GetConfig())
	if err != nil {
		panic(err)
	}
	u.rdb = db.(*redis.Client)

	srvBuilder, found := ucfactory.GetUseCaseBuilder(config.UserSrv)
	if !found {
		panic("user service builder not found")
	}
	srv, err := srvBuilder.(*ucfactory.UserSrvFactory).Build(u.ctn, u.ctn.GetConfig(), config.UserSrv)
	if err != nil {
		panic("user service build error ")
	}

	u.service = srv.(*usecases.UserServicesImpl)
	u.Passwd = []byte("this_is_passwd")
	u.Username = "this_is_username"
}

func (u *UserTestSuite) TearDownSuite() {
	u.db.Delete(&entity.User{Username: u.Username})
	u.rdb.Del(context.Background(), userdata.GetUserCacheKey(u.UserId))
}

func (u *UserTestSuite) TestARegister() {
	user := &model.User{
		Password: u.Passwd,
		Username: u.Username,
	}
	tok, err := u.service.Register(user)
	if err != nil {
		u.FailNowf("cannot register: %s", err.Error())
	}
	u.NotEmpty(tok, "token not generated")
	u.UserId = user.Uid
	userInDb := &entity.User{UserId: user.Uid}
	found, err := u.db.Get(userInDb)
	if err != nil {
		panic(err)
	}
	u.True(found, "user not in mysql")
	u.Equal(user.Password, userInDb.Password, "password not match")
	u.Equal(user.Username, userInDb.Username, "username not match")

	jsdata, err := u.rdb.Get(context.Background(), userdata.GetUserCacheKey(user.Uid)).Result()
	if err != nil {
		panic(err)
	}
	userInRedis, err := unmarshalUser([]byte(jsdata))
	if err != nil {
		panic(err)
	}
	u.Equal(user.Password, userInRedis.Password, "password not match")
	u.Equal(user.Username, userInRedis.Username, "username not match")
}

func (u *UserTestSuite) TestBLogin() {
	user := &model.User{
		Password: u.Passwd,
		Username: u.Username,
	}
	tok, err := u.service.Login(user)
	if err != nil {
		u.FailNowf("cannot login: %s", err.Error())
	}
	u.NotEmpty(tok, "token not generated")

	claim, err := utils.ParseToken(tok)
	if err != nil {
		u.FailNowf("fail to parse token: %s", err.Error())
	}
	u.Equal(user.Uid, claim.UserId)
	u.Token = tok
}

func (u *UserTestSuite) TestCCheckAuth() {
	uid, err := u.service.CheckAuth(u.Token)
	u.NoError(err, "fail to check auth")

	user := &entity.User{UserId: uid}
	found, err := u.db.Get(user)
	u.True(found, "user not found")
	u.NoError(err, "db query fail")

	u.Equal(u.Username, user.Username, "username not match")

	time.Sleep(utils.JWTExpireDuration + time.Second*1)
	_, err = u.service.CheckAuth(u.Token)
	u.EqualError(err, errcode.ErrTokenExpired.String(), "token must expire")
}

func unmarshalUser(jsdata []byte) (*entity.User, error) {
	user := new(entity.User)
	if err := json.Unmarshal(jsdata, user); err != nil {
		return nil, err
	}
	return user, nil
}

func TestUserSuite(t *testing.T) {
	ut := new(UserTestSuite)
	suite.Run(t, ut)
}
