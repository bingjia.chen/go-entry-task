package test

import (
	"context"
	"fmt"
	"mini_market/config"
	"mini_market/container"
	"mini_market/container/srvcontainer"
	"mini_market/container/strgfactory"
	"mini_market/container/ucfactory"
	"mini_market/entity"
	"mini_market/usecases"
	"testing"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/assert"
	"xorm.io/xorm"
)

type CmtTestSuite struct {
	items []*entity.Item
	users []*entity.User
	cmts  [][]*entity.Comment

	ctn     container.Container
	service usecases.CommentService
	db      *xorm.Engine
	rdb     *redis.Client
	As      *assert.Assertions
}

func (i *CmtTestSuite) SetupSuite() {
	i.ctn = srvcontainer.NewServiceContainer()
	if err := i.ctn.InitApp("./config.yaml"); err != nil {
		panic(err)
	}

	dbBuilder, found := strgfactory.GetDataStoreFactory(config.Mysql)
	if !found {
		panic("SQLdb not find")
	}
	db, err := dbBuilder.Build(i.ctn, i.ctn.GetConfig())
	if err != nil {
		panic(err)
	}
	i.db = db.(*xorm.Engine)

	rdbBuilder, found := strgfactory.GetDataStoreFactory(config.Redis)
	if !found {
		panic("redis not find")
	}
	db, err = rdbBuilder.Build(i.ctn, i.ctn.GetConfig())
	if err != nil {
		panic(err)
	}
	i.rdb = db.(*redis.Client)

	srvBuilder, found := ucfactory.GetUseCaseBuilder(config.CmtSrv)
	if !found {
		panic(err)
	}
	srv, err := srvBuilder.(*ucfactory.CmtSrvFactory).Build(i.ctn, i.ctn.GetConfig(), config.CmtSrv)
	if err != nil {
		panic(err)
	}

	i.service = srv.(*usecases.CommentServiceImpl)
	i.prepareData()
}

func (c *CmtTestSuite) TearDownSuite() {
	_, err := c.db.Exec("truncate item;")
	if err != nil {
		fmt.Println(err)
	}

	_, err = c.db.Exec("truncate user;")
	if err != nil {
		fmt.Println(err)
	}

	_, err = c.db.Exec("truncate detail;")
	if err != nil {
		fmt.Println(err)
	}

	_, err = c.db.Exec("truncate comment;")
	if err != nil {
		fmt.Println(err)
	}

	err = c.rdb.FlushAll(context.Background()).Err()
	if err != nil {
		fmt.Println(err)
	}
}

func (c *CmtTestSuite) prepareData() {
	// insert some data
	c.items = []*entity.Item{nil}
	c.users = []*entity.User{nil}
	impl := c.service.(*usecases.CommentServiceImpl)
	var j, nitem, nuser int64
	nuser, nitem = 5, 10
	for j = 0; j < nitem; j++ {
		item := &entity.Item{
			Name: fmt.Sprintf("item %d 😁", j+1),
		}
		detail := &entity.Detail{
			PhotoUrl: "https://picturl_url.cn",
			Desc:     fmt.Sprintf("DETAIL %d", j+1),
		}
		_, err := impl.ItemData.InsertItem(item, detail)
		if err != nil {
			panic(err)
		}
		c.items = append(c.items, item)
	}

	for j = 0; j < nuser; j++ {
		u := &entity.User{
			Username: fmt.Sprintf("user %d", j+1),
			Password: []byte{},
		}
		impl.UserData.InsertUser(u)
		c.users = append(c.users, u)
	}

}

func (c *CmtTestSuite) TestAddComment() {
	c.cmts = [][]*entity.Comment{nil}
	var id, itemId, n int64
	id = 1
	for itemId = 1; itemId < int64(len(c.items)); itemId++ {
		c.cmts = append(c.cmts, []*entity.Comment{})
		for n = 1; n < int64(len(c.users)); n++ {
			cmt := &entity.Comment{}
			cmt.CommentOn = itemId
			cmt.Content = "This is a comment"
			cmt.PublishDate = time.Now().Unix()
			cmt.PublisherId = n
			if id != 1 {
				cmt.ReplyTo = 1
			}
			_, err := c.service.AddComment(cmt.PublisherId, cmt.ReplyTo, cmt.CommentOn, cmt.Content)
			c.As.NoError(err)
			id++
			c.cmts[itemId] = append(c.cmts[itemId], cmt)
		}
	}
	time.Sleep(20 * time.Millisecond)
	count, err := c.rdb.Get(context.Background(), "cmtcount").Int64()
	if err != nil {
		c.As.FailNow("cannot comment count key: %s", err)
	}
	c.As.Equal(count, id-1)
}

func (c *CmtTestSuite) TestGetCommentByItemId() {
	var itemId, page int64
	for itemId = 1; itemId < int64(len(c.items)); itemId++ {
		for page = 0; page < int64(len(c.cmts[0])); page++ {
			cmts, err := c.service.GetCommentOfItem(itemId, page)
			c.As.NoError(err)
			c.As.Equal(c.cmts[itemId][page], cmts[0])
		}
	}
}

func (c *CmtTestSuite) TestGetReply() {
	var cid int64
	cid = 1
	cmts, err := c.service.GetReplyOfComment(cid)
	c.As.NoError(err)
	for _, cmt := range cmts {
		c.As.Equal(int64(1), cmt.ReplyTo)
		c.As.NotEqual(int64(1), cmt.Cid)
	}
}

func TestCmtSuite(t *testing.T) {
	cmt := &CmtTestSuite{}
	cmt.As = assert.New(t)

	cmt.SetupSuite()

	cmt.TestAddComment()
	cmt.TestGetCommentByItemId()
	cmt.TestGetReply()

	cmt.TearDownSuite()
}
