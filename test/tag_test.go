package test

import (
	"mini_market/config"
	"mini_market/container"
	"mini_market/container/srvcontainer"
	"mini_market/container/strgfactory"
	"mini_market/container/ucfactory"
	"mini_market/entity"
	"mini_market/usecases"
	"testing"

	"github.com/stretchr/testify/suite"
	"xorm.io/xorm"
)

type TagTestSuite struct {
	suite.Suite
	items     []*entity.Item
	tags      []*entity.Tag
	tagToItem []map[int64]struct{}
	nPage     int64

	ctn     container.Container
	service usecases.ItemServices
	db      *xorm.Engine
}

func (i *TagTestSuite) SetupSuite() {
	i.items = make([]*entity.Item, 0)
	i.tags = make([]*entity.Tag, 0)
	i.ctn = srvcontainer.NewServiceContainer()
	if err := i.ctn.InitApp("./config.yaml"); err != nil {
		panic(err)
	}

	dbBuilder, found := strgfactory.GetDataStoreFactory(config.Mysql)
	if !found {
		panic("SQLdb not find")
	}
	db, err := dbBuilder.Build(i.ctn, i.ctn.GetConfig())
	if err != nil {
		panic(err)
	}
	i.db = db.(*xorm.Engine)

	srvBuilder, found := ucfactory.GetUseCaseBuilder(config.ItemSrv)
	if !found {
		panic(err)
	}
	srv, err := srvBuilder.(*ucfactory.ItemSrvFactory).Build(i.ctn, i.ctn.GetConfig(), config.ItemSrv)
	if err != nil {
		panic(err)
	}

	i.service = srv.(*usecases.ItemServicesImpl)
	// 10 pages of data
	i.nPage = 10

	// 	impl := i.service.(*usecases.ItemServicesImpl)
	// 	for j := 0; j < 10; j++ {
	// 		tag := &entity.Tag{Name: fmt.Sprintf("tag%d", j)}
	// 		_, err = impl.TagData.InsertTag(tag)
	// 		if err != nil {
	// 			panic(err)
	// 		}
	// 		i.tags = append(i.tags, tag)
	// 		i.tagToItem = append(i.tagToItem, make(map[int64]struct{}))
	// 	}
	//
	// 	// insert some items
	// 	var j int64
	// 	for j = 0; j < i.nPage*i.service.GetCountInPage()+i.service.GetCountInPage()/2; j++ {
	// 		item := &entity.Item{
	// 			Name: fmt.Sprintf("item %d 😁", j+1),
	// 		}
	// 		detail := &entity.Detail{
	// 			PhotoUrl: "https://picturl_url.cn",
	// 			Desc:     fmt.Sprintf("DETAIL %d", j+1),
	// 		}
	//
	// 		tag1 := rand.Int63() % 10
	// 		tag2 := rand.Int63() % 10
	// 		id, err := impl.InsertItem(&model.Detail{
	// 			Name:     item.Name,
	// 			PhotoUrl: detail.PhotoUrl,
	// 			Desc:     detail.PhotoUrl,
	// 		}, []int64{tag1 + 1, tag2 + 1})
	// 		if err != nil {
	// 			panic(err)
	// 		}
	// 		i.items = append(i.items, item)
	// 		i.tagToItem[tag1][id] = struct{}{}
	// 		i.tagToItem[tag2][id] = struct{}{}
	// 	}
	//
}

func (t *TagTestSuite) TearDownSuite() {
	// _, err := t.db.Exec("truncate item")
	// if err != nil {
	// 	t.T().Log(err)
	// }

	// _, err = t.db.Exec("truncate tag")
	// if err != nil {
	// 	t.T().Log(err)
	// }

	// _, err = t.db.Exec("truncate detail")
	// if err != nil {
	// 	t.T().Log(err)
	// }
}

func (t *TagTestSuite) TestAGetItemByTag() {
	impl := t.service.(*usecases.ItemServicesImpl)
	for i := 0; i < 20; i++ {
		err := impl.TagData.AddTagToItem(1, int64(i)+1)
		t.NoError(err)
	}
	tag := &entity.Tag{Name: "tag 1", TagId: 1}
	lastId := int64(0)
	items, next, err := t.service.GetItemsByTags(tag.Name, lastId)
	lastId = next
	t.NoError(err)
	count := 1
	for i := 0; i < len(items); i++ {
		t.Equal(count, items[i].ItemId)
	}
}

func TestTagSuite(t *testing.T) {
	it := new(TagTestSuite)
	suite.Run(t, it)
}
