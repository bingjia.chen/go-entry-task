package test

import (
	"context"
	"fmt"
	"math/rand"
	"mini_market/config"
	"mini_market/container"
	"mini_market/container/srvcontainer"
	"mini_market/container/strgfactory"
	"mini_market/container/ucfactory"
	"mini_market/entity"
	"mini_market/usecases"
	"testing"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/suite"
	"xorm.io/xorm"
)

type ItemTestSuite struct {
	suite.Suite
	items   []*entity.Item
	details []*entity.Detail
	nPage   int64

	ctn     container.Container
	service usecases.ItemServices
	db      *xorm.Engine
	rdb     *redis.Client
}

func (i *ItemTestSuite) SetupSuite() {
	i.items = make([]*entity.Item, 0)
	i.details = make([]*entity.Detail, 0)
	i.ctn = srvcontainer.NewServiceContainer()
	if err := i.ctn.InitApp("./config.yaml"); err != nil {
		panic(err)
	}

	dbBuilder, found := strgfactory.GetDataStoreFactory(config.Mysql)
	if !found {
		panic("SQLdb not find")
	}
	db, err := dbBuilder.Build(i.ctn, i.ctn.GetConfig())
	if err != nil {
		panic(err)
	}
	i.db = db.(*xorm.Engine)

	rdbBuilder, found := strgfactory.GetDataStoreFactory(config.Redis)
	if !found {
		panic("redis not find")
	}
	db, err = rdbBuilder.Build(i.ctn, i.ctn.GetConfig())
	if err != nil {
		panic(err)
	}
	i.rdb = db.(*redis.Client)

	srvBuilder, found := ucfactory.GetUseCaseBuilder(config.ItemSrv)
	if !found {
		panic(err)
	}
	srv, err := srvBuilder.(*ucfactory.ItemSrvFactory).Build(i.ctn, i.ctn.GetConfig(), config.ItemSrv)
	if err != nil {
		panic(err)
	}

	i.service = srv.(*usecases.ItemServicesImpl)
	// 10 pages of data
	i.nPage = 10

	// insert some data
	impl := i.service.(*usecases.ItemServicesImpl)
	var j int64
	for j = 0; j < i.nPage*i.service.GetCountInPage()+i.service.GetCountInPage()/2; j++ {
		item := &entity.Item{
			Name: fmt.Sprintf("item %d 😁", j+1),
		}
		detail := &entity.Detail{
			PhotoUrl: "https://picturl_url.cn",
			Desc:     fmt.Sprintf("DETAIL %d", j+1),
		}
		_, err := impl.ItemData.InsertItem(item, detail)
		if err != nil {
			panic(err)
		}
		i.items = append(i.items, item)
		i.details = append(i.details, detail)
	}
}

func (i *ItemTestSuite) TearDownSuite() {
	_, err := i.db.Exec("truncate item")
	if err != nil {
		i.T().Log(err)
	}

	_, err = i.db.Exec("truncate detail")
	if err != nil {
		i.T().Log(err)
	}
	err = i.rdb.FlushAll(context.Background()).Err()
	if err != nil {
		i.T().Log(err)
	}
}

func (i *ItemTestSuite) TestAGetItemsInCache() {
	var p, pageCount int64
	pageCount = i.service.GetCountInPage()
	i.T().Log("Test getting all items in cache")
	for p = 0; p < i.nPage+1; p++ {
		result, err := i.service.GetItems(p)
		i.NoError(err)
		for j, actual := range result {
			if actual == nil {
				return
			}
			expect := i.items[p*pageCount+int64(j)]
			i.Equal(expect.ItemId, actual.ItemId, "ItemId not match")
			i.Equal(expect.Name, actual.Name, "Name not match")
		}
	}
}

func (i *ItemTestSuite) TestBGetItemsInSql() {
	var p, pageCount int64
	pageCount = i.service.GetCountInPage()
	i.rdb.FlushAll(context.Background())
	i.T().Log("Test getting all items in mysql")
	for p = 0; p < i.nPage+1; p++ {
		result, err := i.service.GetItems(p)
		i.NoError(err)
		for j, actual := range result {
			if actual == nil {
				return
			}
			expect := i.items[p*pageCount+int64(j)]
			i.Equal(expect.ItemId, actual.ItemId, "ItemId not match")
			i.Equal(expect.Name, actual.Name, "Name not match")
		}
	}
	// these items should be in the cache in some time
	time.Sleep(time.Millisecond * 500)
	keys, err := i.rdb.Keys(context.Background(), "item:*").Result()
	if err != nil {
		i.T().Error(err)
	}
	i.Equal(len(i.items), len(keys))
}

func (i *ItemTestSuite) TestCGetItemByName() {
	for j := 0; j < 100; j++ {
		id := rand.Int63n(int64(len(i.items)))
		expect := i.items[id]
		actual, err := i.service.GetItemByName(expect.Name)
		i.NoError(err)
		i.Equal(expect.ItemId, actual.ItemId, "ItemId not match")
		i.Equal(expect.Name, actual.Name, "Name not match")
	}
	i.rdb.FlushAll(context.Background())
	for j := 0; j < 100; j++ {
		id := rand.Int63n(int64(len(i.items)))
		expect := i.items[id]
		actual, err := i.service.GetItemByName(expect.Name)
		i.NoError(err)
		i.Equal(expect.ItemId, actual.ItemId, "ItemId not match")
		i.Equal(expect.Name, actual.Name, "Name not match")
	}
	time.Sleep(time.Millisecond * 500)
	keys, err := i.rdb.Keys(context.Background(), "item:*").Result()
	if err != nil {
		i.T().Error(err)
	}
	i.NotEqual(0, len(keys))
}

func (i *ItemTestSuite) TestDGetDetail() {
	// Get from cache
	for j := 0; j < 100; j++ {
		id := rand.Int63n(int64(len(i.items)))
		expectItem := i.items[id]
		expectDetail := i.details[id]

		actual, err := i.service.GetDetailById(expectItem.ItemId)
		i.NoError(err)
		i.Equal(expectItem.ItemId, actual.ItemId, "ItemId not match")
		i.Equal(expectItem.Name, actual.Name, "Name not match")
		i.Equal(expectDetail.Desc, actual.Desc, "Desc not match")
		i.Equal(expectDetail.PhotoUrl, actual.PhotoUrl, "PhotoUrl not match")
	}

	// Get from database
	i.rdb.FlushAll(context.Background())
	for j := 0; j < 100; j++ {
		id := rand.Int63n(int64(len(i.items)))
		expectItem := i.items[id]
		expectDetail := i.details[id]

		actual, err := i.service.GetDetailById(expectItem.ItemId)
		i.NoError(err)
		i.Equal(expectItem.ItemId, actual.ItemId, "ItemId not match")
		i.Equal(expectItem.Name, actual.Name, "Name not match")

		i.Equal(expectDetail.Desc, actual.Desc, "Desc not match")
		i.Equal(expectDetail.PhotoUrl, actual.PhotoUrl, "PhotoUrl not match")
	}
	time.Sleep(time.Millisecond * 500)
	keys, err := i.rdb.Keys(context.Background(), "detail:*").Result()
	if err != nil {
		i.T().Error(err)
	}
	i.NotEqual(0, len(keys))
}

func TestItemSuite(t *testing.T) {
	it := new(ItemTestSuite)
	suite.Run(t, it)
}
