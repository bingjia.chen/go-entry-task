package entity

// Comment is an entity for user's comment on items
type Comment struct {
	CommentId int64  `xorm:"pk autoincr" json:"comment_id"` // unique id
	Content   string `xorm:"varchar(1024) not null" json:"content"`
	// user Id who publishs this comment
	PublisherId int64 `xorm:"not null" json:"publisher_id"`
	// ReplyTo is the commentId which this comment replies to
	// 0 indicates that this is a first comment
	ReplyTo int64 `xorm:"not null default(0)" json:"reply_to"`
	// CommentOn is the item id which this comment comments on
	CommentOn   int64 `xorm:"not null" json:"comment_on"`
	PublishDate int64 `xorm:"created" json:"publish_date"`
}

func (c *Comment) TableName() string {
	return "comment_tab"
}
