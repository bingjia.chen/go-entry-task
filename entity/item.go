package entity

// Item is an entity for an item
type Item struct {
	ItemId int64  `xorm:"pk autoincr" json:"-"` // unique id
	Name   string `xorm:"varchar(32) unique" json:"name"`
}

type Detail struct {
	DetailId int64  `xorm:"pk autoincr" json:"-"`                    // unique id for an item's details, also for the Item it belongs to
	PhotoUrl string `xorm:"varchar(2083) not null" json:"photo_url"` // photo_url whose maximum length is 2083
	Desc     string `xorm:"text" json:"desc"`                        // detail description
}

func (i *Item) TableName() string {
	return "item_tab"
}

func (d *Detail) TableName() string {
	return "detail_tab"
}
