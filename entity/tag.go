package entity

// Tag is the table for item tag
// NOTE: instance of this struct could be large
type Tag struct {
	TagId int64    `xorm:"tinyint pk autoincr"` // unique id
	Name  string   `xorm:"varchar(32) unique not null"`
	Items []uint64 `xorm:"mediumblob not null"`
}

func (t Tag) TableName() string {
	return "tag_tab"
}
