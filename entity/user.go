package entity

// User is an entity for user
type User struct {
	// user's unique id
	UserId int64 `xorm:"pk autoincr" json:"-"`
	// user's username whose max length is 30
	Username string `xorm:"varchar(32) not null unique" json:"username"`
	// user's password that is encrypted by bcrypt
	Password []byte `xorm:"char(256) not null" json:"password"`
}

func (u User) TableName() string {
	return "user_tab"
}
